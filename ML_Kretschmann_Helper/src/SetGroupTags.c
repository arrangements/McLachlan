#include <cctk.h>
#include <cctk_Parameters.h>
#include <util_Table.h>

#include <assert.h>

static void set_group_tags (int const checkpoint, int const persistent,
                            int const prolongate, char const * restrict const gn)
{
  assert (gn);

  int const gi = CCTK_GroupIndex(gn);
  assert (gi >= 0);

  int const table = CCTK_GroupTagsTableI(gi);
  assert(table >= 0);

  if (!checkpoint) {
    int const ierr = Util_TableSetString (table, "no", "Checkpoint");
    assert (!ierr);
  }

  if (!persistent) {
    int const ierr = Util_TableSetString (table, "no", "Persistent");
    assert (!ierr);
  }

  if (!prolongate) {
    int const iret = Util_TableDeleteKey (table, "ProlongationParameter");
    assert (iret == 0 || iret == UTIL_ERROR_TABLE_NO_SUCH_KEY);
    int const ierr = Util_TableSetString (table, "none", "Prolongation");
    assert (!ierr);
  }
}

int ML_Kretschmann_SetGroupTags (void)
{
  DECLARE_CCTK_PARAMETERS;

  int const checkpoint = 0;

  set_group_tags (checkpoint, checkpoint, 0, "ML_Kretschmann::ML_Kretschmann");

  return 0;
}

