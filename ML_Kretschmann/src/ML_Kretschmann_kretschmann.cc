/*  File produced by Kranc */

#define KRANC_C

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "GenericFD.h"
#include "Differencing.h"
#include "cctk_Loop.h"
#include "loopcontrol.h"
#include "vectors.h"

/* Define macros used in calculations */
#define INITVALUE (42)
#define ScalarINV(x) ((CCTK_REAL)1.0 / (x))
#define ScalarSQR(x) ((x) * (x))
#define ScalarCUB(x) ((x) * ScalarSQR(x))
#define ScalarQAD(x) (ScalarSQR(ScalarSQR(x)))
#define INV(x) (kdiv(ToReal(1.0),x))
#define SQR(x) (kmul(x,x))
#define CUB(x) (kmul(x,SQR(x)))
#define QAD(x) (SQR(SQR(x)))

extern "C" void ML_Kretschmann_kretschmann_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  if (cctk_iteration % ML_Kretschmann_kretschmann_calc_every != ML_Kretschmann_kretschmann_calc_offset)
    return;
  CCTK_INT ierr CCTK_ATTRIBUTE_UNUSED = 0;
  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, GenericFD_GetBoundaryWidth(cctkGH), -1 /* no table */, "ML_Kretschmann::ML_Kretschmann","flat");
  if (ierr < 0)
    CCTK_WARN(1, "Failed to register flat BC for ML_Kretschmann::ML_Kretschmann.");
  return;
}

static void ML_Kretschmann_kretschmann_Body(const cGH* restrict const cctkGH, const int dir, const int face, const CCTK_REAL normal[3], const CCTK_REAL tangentA[3], const CCTK_REAL tangentB[3], const int imin[3], const int imax[3], const int n_subblock_gfs, CCTK_REAL* restrict const subblock_gfs[])
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  
  /* Include user-supplied include files */
  
  /* Initialise finite differencing variables */
  const ptrdiff_t di CCTK_ATTRIBUTE_UNUSED = 1;
  const ptrdiff_t dj CCTK_ATTRIBUTE_UNUSED = CCTK_GFINDEX3D(cctkGH,0,1,0) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t dk CCTK_ATTRIBUTE_UNUSED = CCTK_GFINDEX3D(cctkGH,0,0,1) - CCTK_GFINDEX3D(cctkGH,0,0,0);
  const ptrdiff_t cdi CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * di;
  const ptrdiff_t cdj CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dj;
  const ptrdiff_t cdk CCTK_ATTRIBUTE_UNUSED = sizeof(CCTK_REAL) * dk;
  const CCTK_REAL_VEC dx CCTK_ATTRIBUTE_UNUSED = ToReal(CCTK_DELTA_SPACE(0));
  const CCTK_REAL_VEC dy CCTK_ATTRIBUTE_UNUSED = ToReal(CCTK_DELTA_SPACE(1));
  const CCTK_REAL_VEC dz CCTK_ATTRIBUTE_UNUSED = ToReal(CCTK_DELTA_SPACE(2));
  const CCTK_REAL_VEC dt CCTK_ATTRIBUTE_UNUSED = ToReal(CCTK_DELTA_TIME);
  const CCTK_REAL_VEC t CCTK_ATTRIBUTE_UNUSED = ToReal(cctk_time);
  const CCTK_REAL_VEC dxi CCTK_ATTRIBUTE_UNUSED = INV(dx);
  const CCTK_REAL_VEC dyi CCTK_ATTRIBUTE_UNUSED = INV(dy);
  const CCTK_REAL_VEC dzi CCTK_ATTRIBUTE_UNUSED = INV(dz);
  const CCTK_REAL_VEC khalf CCTK_ATTRIBUTE_UNUSED = ToReal(0.5);
  const CCTK_REAL_VEC kthird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(0.333333333333333333333333333333);
  const CCTK_REAL_VEC ktwothird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(0.666666666666666666666666666667);
  const CCTK_REAL_VEC kfourthird CCTK_ATTRIBUTE_UNUSED = 
    ToReal(1.33333333333333333333333333333);
  const CCTK_REAL_VEC hdxi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dxi,ToReal(0.5));
  const CCTK_REAL_VEC hdyi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dyi,ToReal(0.5));
  const CCTK_REAL_VEC hdzi CCTK_ATTRIBUTE_UNUSED = 
    kmul(dzi,ToReal(0.5));
  
  /* Initialize predefined quantities */
  const CCTK_REAL_VEC p1o12dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0833333333333333333333333333333),dx);
  const CCTK_REAL_VEC p1o12dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0833333333333333333333333333333),dy);
  const CCTK_REAL_VEC p1o12dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0833333333333333333333333333333),dz);
  const CCTK_REAL_VEC p1o144dxdy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00694444444444444444444444444444),kmul(dy,dx));
  const CCTK_REAL_VEC p1o144dxdz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00694444444444444444444444444444),kmul(dz,dx));
  const CCTK_REAL_VEC p1o144dydz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00694444444444444444444444444444),kmul(dz,dy));
  const CCTK_REAL_VEC p1o180dx2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00555555555555555555555555555556),kmul(dx,dx));
  const CCTK_REAL_VEC p1o180dy2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00555555555555555555555555555556),kmul(dy,dy));
  const CCTK_REAL_VEC p1o180dz2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00555555555555555555555555555556),kmul(dz,dz));
  const CCTK_REAL_VEC p1o2dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dx);
  const CCTK_REAL_VEC p1o2dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dy);
  const CCTK_REAL_VEC p1o2dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.5),dz);
  const CCTK_REAL_VEC p1o3600dxdy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000277777777777777777777777777778),kmul(dy,dx));
  const CCTK_REAL_VEC p1o3600dxdz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000277777777777777777777777777778),kmul(dz,dx));
  const CCTK_REAL_VEC p1o3600dydz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000277777777777777777777777777778),kmul(dz,dy));
  const CCTK_REAL_VEC p1o4dxdy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.25),kmul(dy,dx));
  const CCTK_REAL_VEC p1o4dxdz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.25),kmul(dz,dx));
  const CCTK_REAL_VEC p1o4dydz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.25),kmul(dz,dy));
  const CCTK_REAL_VEC p1o5040dx2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dx,dx));
  const CCTK_REAL_VEC p1o5040dy2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dy,dy));
  const CCTK_REAL_VEC p1o5040dz2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.000198412698412698412698412698413),kmul(dz,dz));
  const CCTK_REAL_VEC p1o60dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0166666666666666666666666666667),dx);
  const CCTK_REAL_VEC p1o60dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0166666666666666666666666666667),dy);
  const CCTK_REAL_VEC p1o60dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.0166666666666666666666666666667),dz);
  const CCTK_REAL_VEC p1o705600dxdy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dy,dx));
  const CCTK_REAL_VEC p1o705600dxdz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dz,dx));
  const CCTK_REAL_VEC p1o705600dydz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1.41723356009070294784580498866e-6),kmul(dz,dy));
  const CCTK_REAL_VEC p1o840dx CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dx);
  const CCTK_REAL_VEC p1o840dy CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dy);
  const CCTK_REAL_VEC p1o840dz CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(0.00119047619047619047619047619048),dz);
  const CCTK_REAL_VEC p1odx2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),kmul(dx,dx));
  const CCTK_REAL_VEC p1ody2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),kmul(dy,dy));
  const CCTK_REAL_VEC p1odz2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(1),kmul(dz,dz));
  const CCTK_REAL_VEC pm1o12dx2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.0833333333333333333333333333333),kmul(dx,dx));
  const CCTK_REAL_VEC pm1o12dy2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.0833333333333333333333333333333),kmul(dy,dy));
  const CCTK_REAL_VEC pm1o12dz2 CCTK_ATTRIBUTE_UNUSED = kdiv(ToReal(-0.0833333333333333333333333333333),kmul(dz,dz));
  
  /* Jacobian variable pointers */
  const bool use_jacobian1 = (!CCTK_IsFunctionAliased("MultiPatch_GetMap") || MultiPatch_GetMap(cctkGH) != jacobian_identity_map)
                        && strlen(jacobian_group) > 0;
  const bool use_jacobian = assume_use_jacobian>=0 ? assume_use_jacobian : use_jacobian1;
  const bool usejacobian CCTK_ATTRIBUTE_UNUSED = use_jacobian;
  if (use_jacobian && (strlen(jacobian_derivative_group) == 0))
  {
    CCTK_WARN(1, "GenericFD::jacobian_group and GenericFD::jacobian_derivative_group must both be set to valid group names");
  }
  
  const CCTK_REAL* restrict jacobian_ptrs[9];
  if (use_jacobian) GenericFD_GroupDataPointers(cctkGH, jacobian_group,
                                                9, jacobian_ptrs);
  
  const CCTK_REAL* restrict const J11 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[0] : 0;
  const CCTK_REAL* restrict const J12 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[1] : 0;
  const CCTK_REAL* restrict const J13 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[2] : 0;
  const CCTK_REAL* restrict const J21 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[3] : 0;
  const CCTK_REAL* restrict const J22 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[4] : 0;
  const CCTK_REAL* restrict const J23 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[5] : 0;
  const CCTK_REAL* restrict const J31 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[6] : 0;
  const CCTK_REAL* restrict const J32 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[7] : 0;
  const CCTK_REAL* restrict const J33 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_ptrs[8] : 0;
  
  const CCTK_REAL* restrict jacobian_derivative_ptrs[18] CCTK_ATTRIBUTE_UNUSED;
  if (use_jacobian) GenericFD_GroupDataPointers(cctkGH, jacobian_derivative_group,
                                                18, jacobian_derivative_ptrs);
  
  const CCTK_REAL* restrict const dJ111 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[0] : 0;
  const CCTK_REAL* restrict const dJ112 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[1] : 0;
  const CCTK_REAL* restrict const dJ113 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[2] : 0;
  const CCTK_REAL* restrict const dJ122 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[3] : 0;
  const CCTK_REAL* restrict const dJ123 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[4] : 0;
  const CCTK_REAL* restrict const dJ133 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[5] : 0;
  const CCTK_REAL* restrict const dJ211 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[6] : 0;
  const CCTK_REAL* restrict const dJ212 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[7] : 0;
  const CCTK_REAL* restrict const dJ213 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[8] : 0;
  const CCTK_REAL* restrict const dJ222 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[9] : 0;
  const CCTK_REAL* restrict const dJ223 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[10] : 0;
  const CCTK_REAL* restrict const dJ233 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[11] : 0;
  const CCTK_REAL* restrict const dJ311 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[12] : 0;
  const CCTK_REAL* restrict const dJ312 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[13] : 0;
  const CCTK_REAL* restrict const dJ313 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[14] : 0;
  const CCTK_REAL* restrict const dJ322 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[15] : 0;
  const CCTK_REAL* restrict const dJ323 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[16] : 0;
  const CCTK_REAL* restrict const dJ333 CCTK_ATTRIBUTE_UNUSED = use_jacobian ? jacobian_derivative_ptrs[17] : 0;
  
  /* Assign local copies of arrays functions */
  
  
  
  /* Calculate temporaries and arrays functions */
  
  /* Copy local copies back to grid functions */
  
  /* Loop over the grid points */
  const int imin0=imin[0];
  const int imin1=imin[1];
  const int imin2=imin[2];
  const int imax0=imax[0];
  const int imax1=imax[1];
  const int imax2=imax[2];
  #pragma omp parallel // reduction(+: vec_iter_counter, vec_op_counter, vec_mem_counter)
  CCTK_LOOP3STR(ML_Kretschmann_kretschmann,
    i,j,k, imin0,imin1,imin2, imax0,imax1,imax2,
    cctk_ash[0],cctk_ash[1],cctk_ash[2],
    vecimin,vecimax, CCTK_REAL_VEC_SIZE)
  {
    const ptrdiff_t index CCTK_ATTRIBUTE_UNUSED = di*i + dj*j + dk*k;
    // vec_iter_counter+=CCTK_REAL_VEC_SIZE;
    
    /* Assign local copies of grid functions */
    
    CCTK_REAL_VEC At11L CCTK_ATTRIBUTE_UNUSED = vec_load(At11[index]);
    CCTK_REAL_VEC At12L CCTK_ATTRIBUTE_UNUSED = vec_load(At12[index]);
    CCTK_REAL_VEC At13L CCTK_ATTRIBUTE_UNUSED = vec_load(At13[index]);
    CCTK_REAL_VEC At22L CCTK_ATTRIBUTE_UNUSED = vec_load(At22[index]);
    CCTK_REAL_VEC At23L CCTK_ATTRIBUTE_UNUSED = vec_load(At23[index]);
    CCTK_REAL_VEC At33L CCTK_ATTRIBUTE_UNUSED = vec_load(At33[index]);
    CCTK_REAL_VEC gt11L CCTK_ATTRIBUTE_UNUSED = vec_load(gt11[index]);
    CCTK_REAL_VEC gt12L CCTK_ATTRIBUTE_UNUSED = vec_load(gt12[index]);
    CCTK_REAL_VEC gt13L CCTK_ATTRIBUTE_UNUSED = vec_load(gt13[index]);
    CCTK_REAL_VEC gt22L CCTK_ATTRIBUTE_UNUSED = vec_load(gt22[index]);
    CCTK_REAL_VEC gt23L CCTK_ATTRIBUTE_UNUSED = vec_load(gt23[index]);
    CCTK_REAL_VEC gt33L CCTK_ATTRIBUTE_UNUSED = vec_load(gt33[index]);
    CCTK_REAL_VEC phiL CCTK_ATTRIBUTE_UNUSED = vec_load(phi[index]);
    CCTK_REAL_VEC trKL CCTK_ATTRIBUTE_UNUSED = vec_load(trK[index]);
    
    
    CCTK_REAL_VEC dJ111L, dJ112L, dJ113L, dJ122L, dJ123L, dJ133L, dJ211L, dJ212L, dJ213L, dJ222L, dJ223L, dJ233L, dJ311L, dJ312L, dJ313L, dJ322L, dJ323L, dJ333L, J11L, J12L, J13L, J21L, J22L, J23L, J31L, J32L, J33L CCTK_ATTRIBUTE_UNUSED ;
    
    if (use_jacobian)
    {
      dJ111L = vec_load(dJ111[index]);
      dJ112L = vec_load(dJ112[index]);
      dJ113L = vec_load(dJ113[index]);
      dJ122L = vec_load(dJ122[index]);
      dJ123L = vec_load(dJ123[index]);
      dJ133L = vec_load(dJ133[index]);
      dJ211L = vec_load(dJ211[index]);
      dJ212L = vec_load(dJ212[index]);
      dJ213L = vec_load(dJ213[index]);
      dJ222L = vec_load(dJ222[index]);
      dJ223L = vec_load(dJ223[index]);
      dJ233L = vec_load(dJ233[index]);
      dJ311L = vec_load(dJ311[index]);
      dJ312L = vec_load(dJ312[index]);
      dJ313L = vec_load(dJ313[index]);
      dJ322L = vec_load(dJ322[index]);
      dJ323L = vec_load(dJ323[index]);
      dJ333L = vec_load(dJ333[index]);
      J11L = vec_load(J11[index]);
      J12L = vec_load(J12[index]);
      J13L = vec_load(J13[index]);
      J21L = vec_load(J21[index]);
      J22L = vec_load(J22[index]);
      J23L = vec_load(J23[index]);
      J31L = vec_load(J31[index]);
      J32L = vec_load(J32[index]);
      J33L = vec_load(J33[index]);
    }
    
    /* Include user supplied include files */
    
    /* Precompute derivatives */
    CCTK_REAL_VEC PDstandardNth1At11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2At11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3At11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1At12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2At12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3At12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1At13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2At13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3At13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1At22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2At22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3At22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1At23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2At23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3At23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1At33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2At33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3At33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth11gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth22gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth33gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth12gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth13gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth23gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth11gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth22gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth33gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth12gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth13gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth23gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth11gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth22gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth33gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth12gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth13gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth23gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth11gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth22gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth33gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth12gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth13gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth23gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth11gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth22gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth33gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth12gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth13gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth23gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth11gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth22gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth33gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth12gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth13gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth23gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth11phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth22phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth33phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth12phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth13phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth23phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth1trK CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth2trK CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC PDstandardNth3trK CCTK_ATTRIBUTE_UNUSED;
    
    switch (fdOrder)
    {
      case 2:
      {
        PDstandardNth1At11 = PDstandardNthfdOrder21(&At11[index]);
        PDstandardNth2At11 = PDstandardNthfdOrder22(&At11[index]);
        PDstandardNth3At11 = PDstandardNthfdOrder23(&At11[index]);
        PDstandardNth1At12 = PDstandardNthfdOrder21(&At12[index]);
        PDstandardNth2At12 = PDstandardNthfdOrder22(&At12[index]);
        PDstandardNth3At12 = PDstandardNthfdOrder23(&At12[index]);
        PDstandardNth1At13 = PDstandardNthfdOrder21(&At13[index]);
        PDstandardNth2At13 = PDstandardNthfdOrder22(&At13[index]);
        PDstandardNth3At13 = PDstandardNthfdOrder23(&At13[index]);
        PDstandardNth1At22 = PDstandardNthfdOrder21(&At22[index]);
        PDstandardNth2At22 = PDstandardNthfdOrder22(&At22[index]);
        PDstandardNth3At22 = PDstandardNthfdOrder23(&At22[index]);
        PDstandardNth1At23 = PDstandardNthfdOrder21(&At23[index]);
        PDstandardNth2At23 = PDstandardNthfdOrder22(&At23[index]);
        PDstandardNth3At23 = PDstandardNthfdOrder23(&At23[index]);
        PDstandardNth1At33 = PDstandardNthfdOrder21(&At33[index]);
        PDstandardNth2At33 = PDstandardNthfdOrder22(&At33[index]);
        PDstandardNth3At33 = PDstandardNthfdOrder23(&At33[index]);
        PDstandardNth1gt11 = PDstandardNthfdOrder21(&gt11[index]);
        PDstandardNth2gt11 = PDstandardNthfdOrder22(&gt11[index]);
        PDstandardNth3gt11 = PDstandardNthfdOrder23(&gt11[index]);
        PDstandardNth11gt11 = PDstandardNthfdOrder211(&gt11[index]);
        PDstandardNth22gt11 = PDstandardNthfdOrder222(&gt11[index]);
        PDstandardNth33gt11 = PDstandardNthfdOrder233(&gt11[index]);
        PDstandardNth12gt11 = PDstandardNthfdOrder212(&gt11[index]);
        PDstandardNth13gt11 = PDstandardNthfdOrder213(&gt11[index]);
        PDstandardNth23gt11 = PDstandardNthfdOrder223(&gt11[index]);
        PDstandardNth1gt12 = PDstandardNthfdOrder21(&gt12[index]);
        PDstandardNth2gt12 = PDstandardNthfdOrder22(&gt12[index]);
        PDstandardNth3gt12 = PDstandardNthfdOrder23(&gt12[index]);
        PDstandardNth11gt12 = PDstandardNthfdOrder211(&gt12[index]);
        PDstandardNth22gt12 = PDstandardNthfdOrder222(&gt12[index]);
        PDstandardNth33gt12 = PDstandardNthfdOrder233(&gt12[index]);
        PDstandardNth12gt12 = PDstandardNthfdOrder212(&gt12[index]);
        PDstandardNth13gt12 = PDstandardNthfdOrder213(&gt12[index]);
        PDstandardNth23gt12 = PDstandardNthfdOrder223(&gt12[index]);
        PDstandardNth1gt13 = PDstandardNthfdOrder21(&gt13[index]);
        PDstandardNth2gt13 = PDstandardNthfdOrder22(&gt13[index]);
        PDstandardNth3gt13 = PDstandardNthfdOrder23(&gt13[index]);
        PDstandardNth11gt13 = PDstandardNthfdOrder211(&gt13[index]);
        PDstandardNth22gt13 = PDstandardNthfdOrder222(&gt13[index]);
        PDstandardNth33gt13 = PDstandardNthfdOrder233(&gt13[index]);
        PDstandardNth12gt13 = PDstandardNthfdOrder212(&gt13[index]);
        PDstandardNth13gt13 = PDstandardNthfdOrder213(&gt13[index]);
        PDstandardNth23gt13 = PDstandardNthfdOrder223(&gt13[index]);
        PDstandardNth1gt22 = PDstandardNthfdOrder21(&gt22[index]);
        PDstandardNth2gt22 = PDstandardNthfdOrder22(&gt22[index]);
        PDstandardNth3gt22 = PDstandardNthfdOrder23(&gt22[index]);
        PDstandardNth11gt22 = PDstandardNthfdOrder211(&gt22[index]);
        PDstandardNth22gt22 = PDstandardNthfdOrder222(&gt22[index]);
        PDstandardNth33gt22 = PDstandardNthfdOrder233(&gt22[index]);
        PDstandardNth12gt22 = PDstandardNthfdOrder212(&gt22[index]);
        PDstandardNth13gt22 = PDstandardNthfdOrder213(&gt22[index]);
        PDstandardNth23gt22 = PDstandardNthfdOrder223(&gt22[index]);
        PDstandardNth1gt23 = PDstandardNthfdOrder21(&gt23[index]);
        PDstandardNth2gt23 = PDstandardNthfdOrder22(&gt23[index]);
        PDstandardNth3gt23 = PDstandardNthfdOrder23(&gt23[index]);
        PDstandardNth11gt23 = PDstandardNthfdOrder211(&gt23[index]);
        PDstandardNth22gt23 = PDstandardNthfdOrder222(&gt23[index]);
        PDstandardNth33gt23 = PDstandardNthfdOrder233(&gt23[index]);
        PDstandardNth12gt23 = PDstandardNthfdOrder212(&gt23[index]);
        PDstandardNth13gt23 = PDstandardNthfdOrder213(&gt23[index]);
        PDstandardNth23gt23 = PDstandardNthfdOrder223(&gt23[index]);
        PDstandardNth1gt33 = PDstandardNthfdOrder21(&gt33[index]);
        PDstandardNth2gt33 = PDstandardNthfdOrder22(&gt33[index]);
        PDstandardNth3gt33 = PDstandardNthfdOrder23(&gt33[index]);
        PDstandardNth11gt33 = PDstandardNthfdOrder211(&gt33[index]);
        PDstandardNth22gt33 = PDstandardNthfdOrder222(&gt33[index]);
        PDstandardNth33gt33 = PDstandardNthfdOrder233(&gt33[index]);
        PDstandardNth12gt33 = PDstandardNthfdOrder212(&gt33[index]);
        PDstandardNth13gt33 = PDstandardNthfdOrder213(&gt33[index]);
        PDstandardNth23gt33 = PDstandardNthfdOrder223(&gt33[index]);
        PDstandardNth1phi = PDstandardNthfdOrder21(&phi[index]);
        PDstandardNth2phi = PDstandardNthfdOrder22(&phi[index]);
        PDstandardNth3phi = PDstandardNthfdOrder23(&phi[index]);
        PDstandardNth11phi = PDstandardNthfdOrder211(&phi[index]);
        PDstandardNth22phi = PDstandardNthfdOrder222(&phi[index]);
        PDstandardNth33phi = PDstandardNthfdOrder233(&phi[index]);
        PDstandardNth12phi = PDstandardNthfdOrder212(&phi[index]);
        PDstandardNth13phi = PDstandardNthfdOrder213(&phi[index]);
        PDstandardNth23phi = PDstandardNthfdOrder223(&phi[index]);
        PDstandardNth1trK = PDstandardNthfdOrder21(&trK[index]);
        PDstandardNth2trK = PDstandardNthfdOrder22(&trK[index]);
        PDstandardNth3trK = PDstandardNthfdOrder23(&trK[index]);
        break;
      }
      
      case 4:
      {
        PDstandardNth1At11 = PDstandardNthfdOrder41(&At11[index]);
        PDstandardNth2At11 = PDstandardNthfdOrder42(&At11[index]);
        PDstandardNth3At11 = PDstandardNthfdOrder43(&At11[index]);
        PDstandardNth1At12 = PDstandardNthfdOrder41(&At12[index]);
        PDstandardNth2At12 = PDstandardNthfdOrder42(&At12[index]);
        PDstandardNth3At12 = PDstandardNthfdOrder43(&At12[index]);
        PDstandardNth1At13 = PDstandardNthfdOrder41(&At13[index]);
        PDstandardNth2At13 = PDstandardNthfdOrder42(&At13[index]);
        PDstandardNth3At13 = PDstandardNthfdOrder43(&At13[index]);
        PDstandardNth1At22 = PDstandardNthfdOrder41(&At22[index]);
        PDstandardNth2At22 = PDstandardNthfdOrder42(&At22[index]);
        PDstandardNth3At22 = PDstandardNthfdOrder43(&At22[index]);
        PDstandardNth1At23 = PDstandardNthfdOrder41(&At23[index]);
        PDstandardNth2At23 = PDstandardNthfdOrder42(&At23[index]);
        PDstandardNth3At23 = PDstandardNthfdOrder43(&At23[index]);
        PDstandardNth1At33 = PDstandardNthfdOrder41(&At33[index]);
        PDstandardNth2At33 = PDstandardNthfdOrder42(&At33[index]);
        PDstandardNth3At33 = PDstandardNthfdOrder43(&At33[index]);
        PDstandardNth1gt11 = PDstandardNthfdOrder41(&gt11[index]);
        PDstandardNth2gt11 = PDstandardNthfdOrder42(&gt11[index]);
        PDstandardNth3gt11 = PDstandardNthfdOrder43(&gt11[index]);
        PDstandardNth11gt11 = PDstandardNthfdOrder411(&gt11[index]);
        PDstandardNth22gt11 = PDstandardNthfdOrder422(&gt11[index]);
        PDstandardNth33gt11 = PDstandardNthfdOrder433(&gt11[index]);
        PDstandardNth12gt11 = PDstandardNthfdOrder412(&gt11[index]);
        PDstandardNth13gt11 = PDstandardNthfdOrder413(&gt11[index]);
        PDstandardNth23gt11 = PDstandardNthfdOrder423(&gt11[index]);
        PDstandardNth1gt12 = PDstandardNthfdOrder41(&gt12[index]);
        PDstandardNth2gt12 = PDstandardNthfdOrder42(&gt12[index]);
        PDstandardNth3gt12 = PDstandardNthfdOrder43(&gt12[index]);
        PDstandardNth11gt12 = PDstandardNthfdOrder411(&gt12[index]);
        PDstandardNth22gt12 = PDstandardNthfdOrder422(&gt12[index]);
        PDstandardNth33gt12 = PDstandardNthfdOrder433(&gt12[index]);
        PDstandardNth12gt12 = PDstandardNthfdOrder412(&gt12[index]);
        PDstandardNth13gt12 = PDstandardNthfdOrder413(&gt12[index]);
        PDstandardNth23gt12 = PDstandardNthfdOrder423(&gt12[index]);
        PDstandardNth1gt13 = PDstandardNthfdOrder41(&gt13[index]);
        PDstandardNth2gt13 = PDstandardNthfdOrder42(&gt13[index]);
        PDstandardNth3gt13 = PDstandardNthfdOrder43(&gt13[index]);
        PDstandardNth11gt13 = PDstandardNthfdOrder411(&gt13[index]);
        PDstandardNth22gt13 = PDstandardNthfdOrder422(&gt13[index]);
        PDstandardNth33gt13 = PDstandardNthfdOrder433(&gt13[index]);
        PDstandardNth12gt13 = PDstandardNthfdOrder412(&gt13[index]);
        PDstandardNth13gt13 = PDstandardNthfdOrder413(&gt13[index]);
        PDstandardNth23gt13 = PDstandardNthfdOrder423(&gt13[index]);
        PDstandardNth1gt22 = PDstandardNthfdOrder41(&gt22[index]);
        PDstandardNth2gt22 = PDstandardNthfdOrder42(&gt22[index]);
        PDstandardNth3gt22 = PDstandardNthfdOrder43(&gt22[index]);
        PDstandardNth11gt22 = PDstandardNthfdOrder411(&gt22[index]);
        PDstandardNth22gt22 = PDstandardNthfdOrder422(&gt22[index]);
        PDstandardNth33gt22 = PDstandardNthfdOrder433(&gt22[index]);
        PDstandardNth12gt22 = PDstandardNthfdOrder412(&gt22[index]);
        PDstandardNth13gt22 = PDstandardNthfdOrder413(&gt22[index]);
        PDstandardNth23gt22 = PDstandardNthfdOrder423(&gt22[index]);
        PDstandardNth1gt23 = PDstandardNthfdOrder41(&gt23[index]);
        PDstandardNth2gt23 = PDstandardNthfdOrder42(&gt23[index]);
        PDstandardNth3gt23 = PDstandardNthfdOrder43(&gt23[index]);
        PDstandardNth11gt23 = PDstandardNthfdOrder411(&gt23[index]);
        PDstandardNth22gt23 = PDstandardNthfdOrder422(&gt23[index]);
        PDstandardNth33gt23 = PDstandardNthfdOrder433(&gt23[index]);
        PDstandardNth12gt23 = PDstandardNthfdOrder412(&gt23[index]);
        PDstandardNth13gt23 = PDstandardNthfdOrder413(&gt23[index]);
        PDstandardNth23gt23 = PDstandardNthfdOrder423(&gt23[index]);
        PDstandardNth1gt33 = PDstandardNthfdOrder41(&gt33[index]);
        PDstandardNth2gt33 = PDstandardNthfdOrder42(&gt33[index]);
        PDstandardNth3gt33 = PDstandardNthfdOrder43(&gt33[index]);
        PDstandardNth11gt33 = PDstandardNthfdOrder411(&gt33[index]);
        PDstandardNth22gt33 = PDstandardNthfdOrder422(&gt33[index]);
        PDstandardNth33gt33 = PDstandardNthfdOrder433(&gt33[index]);
        PDstandardNth12gt33 = PDstandardNthfdOrder412(&gt33[index]);
        PDstandardNth13gt33 = PDstandardNthfdOrder413(&gt33[index]);
        PDstandardNth23gt33 = PDstandardNthfdOrder423(&gt33[index]);
        PDstandardNth1phi = PDstandardNthfdOrder41(&phi[index]);
        PDstandardNth2phi = PDstandardNthfdOrder42(&phi[index]);
        PDstandardNth3phi = PDstandardNthfdOrder43(&phi[index]);
        PDstandardNth11phi = PDstandardNthfdOrder411(&phi[index]);
        PDstandardNth22phi = PDstandardNthfdOrder422(&phi[index]);
        PDstandardNth33phi = PDstandardNthfdOrder433(&phi[index]);
        PDstandardNth12phi = PDstandardNthfdOrder412(&phi[index]);
        PDstandardNth13phi = PDstandardNthfdOrder413(&phi[index]);
        PDstandardNth23phi = PDstandardNthfdOrder423(&phi[index]);
        PDstandardNth1trK = PDstandardNthfdOrder41(&trK[index]);
        PDstandardNth2trK = PDstandardNthfdOrder42(&trK[index]);
        PDstandardNth3trK = PDstandardNthfdOrder43(&trK[index]);
        break;
      }
      
      case 6:
      {
        PDstandardNth1At11 = PDstandardNthfdOrder61(&At11[index]);
        PDstandardNth2At11 = PDstandardNthfdOrder62(&At11[index]);
        PDstandardNth3At11 = PDstandardNthfdOrder63(&At11[index]);
        PDstandardNth1At12 = PDstandardNthfdOrder61(&At12[index]);
        PDstandardNth2At12 = PDstandardNthfdOrder62(&At12[index]);
        PDstandardNth3At12 = PDstandardNthfdOrder63(&At12[index]);
        PDstandardNth1At13 = PDstandardNthfdOrder61(&At13[index]);
        PDstandardNth2At13 = PDstandardNthfdOrder62(&At13[index]);
        PDstandardNth3At13 = PDstandardNthfdOrder63(&At13[index]);
        PDstandardNth1At22 = PDstandardNthfdOrder61(&At22[index]);
        PDstandardNth2At22 = PDstandardNthfdOrder62(&At22[index]);
        PDstandardNth3At22 = PDstandardNthfdOrder63(&At22[index]);
        PDstandardNth1At23 = PDstandardNthfdOrder61(&At23[index]);
        PDstandardNth2At23 = PDstandardNthfdOrder62(&At23[index]);
        PDstandardNth3At23 = PDstandardNthfdOrder63(&At23[index]);
        PDstandardNth1At33 = PDstandardNthfdOrder61(&At33[index]);
        PDstandardNth2At33 = PDstandardNthfdOrder62(&At33[index]);
        PDstandardNth3At33 = PDstandardNthfdOrder63(&At33[index]);
        PDstandardNth1gt11 = PDstandardNthfdOrder61(&gt11[index]);
        PDstandardNth2gt11 = PDstandardNthfdOrder62(&gt11[index]);
        PDstandardNth3gt11 = PDstandardNthfdOrder63(&gt11[index]);
        PDstandardNth11gt11 = PDstandardNthfdOrder611(&gt11[index]);
        PDstandardNth22gt11 = PDstandardNthfdOrder622(&gt11[index]);
        PDstandardNth33gt11 = PDstandardNthfdOrder633(&gt11[index]);
        PDstandardNth12gt11 = PDstandardNthfdOrder612(&gt11[index]);
        PDstandardNth13gt11 = PDstandardNthfdOrder613(&gt11[index]);
        PDstandardNth23gt11 = PDstandardNthfdOrder623(&gt11[index]);
        PDstandardNth1gt12 = PDstandardNthfdOrder61(&gt12[index]);
        PDstandardNth2gt12 = PDstandardNthfdOrder62(&gt12[index]);
        PDstandardNth3gt12 = PDstandardNthfdOrder63(&gt12[index]);
        PDstandardNth11gt12 = PDstandardNthfdOrder611(&gt12[index]);
        PDstandardNth22gt12 = PDstandardNthfdOrder622(&gt12[index]);
        PDstandardNth33gt12 = PDstandardNthfdOrder633(&gt12[index]);
        PDstandardNth12gt12 = PDstandardNthfdOrder612(&gt12[index]);
        PDstandardNth13gt12 = PDstandardNthfdOrder613(&gt12[index]);
        PDstandardNth23gt12 = PDstandardNthfdOrder623(&gt12[index]);
        PDstandardNth1gt13 = PDstandardNthfdOrder61(&gt13[index]);
        PDstandardNth2gt13 = PDstandardNthfdOrder62(&gt13[index]);
        PDstandardNth3gt13 = PDstandardNthfdOrder63(&gt13[index]);
        PDstandardNth11gt13 = PDstandardNthfdOrder611(&gt13[index]);
        PDstandardNth22gt13 = PDstandardNthfdOrder622(&gt13[index]);
        PDstandardNth33gt13 = PDstandardNthfdOrder633(&gt13[index]);
        PDstandardNth12gt13 = PDstandardNthfdOrder612(&gt13[index]);
        PDstandardNth13gt13 = PDstandardNthfdOrder613(&gt13[index]);
        PDstandardNth23gt13 = PDstandardNthfdOrder623(&gt13[index]);
        PDstandardNth1gt22 = PDstandardNthfdOrder61(&gt22[index]);
        PDstandardNth2gt22 = PDstandardNthfdOrder62(&gt22[index]);
        PDstandardNth3gt22 = PDstandardNthfdOrder63(&gt22[index]);
        PDstandardNth11gt22 = PDstandardNthfdOrder611(&gt22[index]);
        PDstandardNth22gt22 = PDstandardNthfdOrder622(&gt22[index]);
        PDstandardNth33gt22 = PDstandardNthfdOrder633(&gt22[index]);
        PDstandardNth12gt22 = PDstandardNthfdOrder612(&gt22[index]);
        PDstandardNth13gt22 = PDstandardNthfdOrder613(&gt22[index]);
        PDstandardNth23gt22 = PDstandardNthfdOrder623(&gt22[index]);
        PDstandardNth1gt23 = PDstandardNthfdOrder61(&gt23[index]);
        PDstandardNth2gt23 = PDstandardNthfdOrder62(&gt23[index]);
        PDstandardNth3gt23 = PDstandardNthfdOrder63(&gt23[index]);
        PDstandardNth11gt23 = PDstandardNthfdOrder611(&gt23[index]);
        PDstandardNth22gt23 = PDstandardNthfdOrder622(&gt23[index]);
        PDstandardNth33gt23 = PDstandardNthfdOrder633(&gt23[index]);
        PDstandardNth12gt23 = PDstandardNthfdOrder612(&gt23[index]);
        PDstandardNth13gt23 = PDstandardNthfdOrder613(&gt23[index]);
        PDstandardNth23gt23 = PDstandardNthfdOrder623(&gt23[index]);
        PDstandardNth1gt33 = PDstandardNthfdOrder61(&gt33[index]);
        PDstandardNth2gt33 = PDstandardNthfdOrder62(&gt33[index]);
        PDstandardNth3gt33 = PDstandardNthfdOrder63(&gt33[index]);
        PDstandardNth11gt33 = PDstandardNthfdOrder611(&gt33[index]);
        PDstandardNth22gt33 = PDstandardNthfdOrder622(&gt33[index]);
        PDstandardNth33gt33 = PDstandardNthfdOrder633(&gt33[index]);
        PDstandardNth12gt33 = PDstandardNthfdOrder612(&gt33[index]);
        PDstandardNth13gt33 = PDstandardNthfdOrder613(&gt33[index]);
        PDstandardNth23gt33 = PDstandardNthfdOrder623(&gt33[index]);
        PDstandardNth1phi = PDstandardNthfdOrder61(&phi[index]);
        PDstandardNth2phi = PDstandardNthfdOrder62(&phi[index]);
        PDstandardNth3phi = PDstandardNthfdOrder63(&phi[index]);
        PDstandardNth11phi = PDstandardNthfdOrder611(&phi[index]);
        PDstandardNth22phi = PDstandardNthfdOrder622(&phi[index]);
        PDstandardNth33phi = PDstandardNthfdOrder633(&phi[index]);
        PDstandardNth12phi = PDstandardNthfdOrder612(&phi[index]);
        PDstandardNth13phi = PDstandardNthfdOrder613(&phi[index]);
        PDstandardNth23phi = PDstandardNthfdOrder623(&phi[index]);
        PDstandardNth1trK = PDstandardNthfdOrder61(&trK[index]);
        PDstandardNth2trK = PDstandardNthfdOrder62(&trK[index]);
        PDstandardNth3trK = PDstandardNthfdOrder63(&trK[index]);
        break;
      }
      
      case 8:
      {
        PDstandardNth1At11 = PDstandardNthfdOrder81(&At11[index]);
        PDstandardNth2At11 = PDstandardNthfdOrder82(&At11[index]);
        PDstandardNth3At11 = PDstandardNthfdOrder83(&At11[index]);
        PDstandardNth1At12 = PDstandardNthfdOrder81(&At12[index]);
        PDstandardNth2At12 = PDstandardNthfdOrder82(&At12[index]);
        PDstandardNth3At12 = PDstandardNthfdOrder83(&At12[index]);
        PDstandardNth1At13 = PDstandardNthfdOrder81(&At13[index]);
        PDstandardNth2At13 = PDstandardNthfdOrder82(&At13[index]);
        PDstandardNth3At13 = PDstandardNthfdOrder83(&At13[index]);
        PDstandardNth1At22 = PDstandardNthfdOrder81(&At22[index]);
        PDstandardNth2At22 = PDstandardNthfdOrder82(&At22[index]);
        PDstandardNth3At22 = PDstandardNthfdOrder83(&At22[index]);
        PDstandardNth1At23 = PDstandardNthfdOrder81(&At23[index]);
        PDstandardNth2At23 = PDstandardNthfdOrder82(&At23[index]);
        PDstandardNth3At23 = PDstandardNthfdOrder83(&At23[index]);
        PDstandardNth1At33 = PDstandardNthfdOrder81(&At33[index]);
        PDstandardNth2At33 = PDstandardNthfdOrder82(&At33[index]);
        PDstandardNth3At33 = PDstandardNthfdOrder83(&At33[index]);
        PDstandardNth1gt11 = PDstandardNthfdOrder81(&gt11[index]);
        PDstandardNth2gt11 = PDstandardNthfdOrder82(&gt11[index]);
        PDstandardNth3gt11 = PDstandardNthfdOrder83(&gt11[index]);
        PDstandardNth11gt11 = PDstandardNthfdOrder811(&gt11[index]);
        PDstandardNth22gt11 = PDstandardNthfdOrder822(&gt11[index]);
        PDstandardNth33gt11 = PDstandardNthfdOrder833(&gt11[index]);
        PDstandardNth12gt11 = PDstandardNthfdOrder812(&gt11[index]);
        PDstandardNth13gt11 = PDstandardNthfdOrder813(&gt11[index]);
        PDstandardNth23gt11 = PDstandardNthfdOrder823(&gt11[index]);
        PDstandardNth1gt12 = PDstandardNthfdOrder81(&gt12[index]);
        PDstandardNth2gt12 = PDstandardNthfdOrder82(&gt12[index]);
        PDstandardNth3gt12 = PDstandardNthfdOrder83(&gt12[index]);
        PDstandardNth11gt12 = PDstandardNthfdOrder811(&gt12[index]);
        PDstandardNth22gt12 = PDstandardNthfdOrder822(&gt12[index]);
        PDstandardNth33gt12 = PDstandardNthfdOrder833(&gt12[index]);
        PDstandardNth12gt12 = PDstandardNthfdOrder812(&gt12[index]);
        PDstandardNth13gt12 = PDstandardNthfdOrder813(&gt12[index]);
        PDstandardNth23gt12 = PDstandardNthfdOrder823(&gt12[index]);
        PDstandardNth1gt13 = PDstandardNthfdOrder81(&gt13[index]);
        PDstandardNth2gt13 = PDstandardNthfdOrder82(&gt13[index]);
        PDstandardNth3gt13 = PDstandardNthfdOrder83(&gt13[index]);
        PDstandardNth11gt13 = PDstandardNthfdOrder811(&gt13[index]);
        PDstandardNth22gt13 = PDstandardNthfdOrder822(&gt13[index]);
        PDstandardNth33gt13 = PDstandardNthfdOrder833(&gt13[index]);
        PDstandardNth12gt13 = PDstandardNthfdOrder812(&gt13[index]);
        PDstandardNth13gt13 = PDstandardNthfdOrder813(&gt13[index]);
        PDstandardNth23gt13 = PDstandardNthfdOrder823(&gt13[index]);
        PDstandardNth1gt22 = PDstandardNthfdOrder81(&gt22[index]);
        PDstandardNth2gt22 = PDstandardNthfdOrder82(&gt22[index]);
        PDstandardNth3gt22 = PDstandardNthfdOrder83(&gt22[index]);
        PDstandardNth11gt22 = PDstandardNthfdOrder811(&gt22[index]);
        PDstandardNth22gt22 = PDstandardNthfdOrder822(&gt22[index]);
        PDstandardNth33gt22 = PDstandardNthfdOrder833(&gt22[index]);
        PDstandardNth12gt22 = PDstandardNthfdOrder812(&gt22[index]);
        PDstandardNth13gt22 = PDstandardNthfdOrder813(&gt22[index]);
        PDstandardNth23gt22 = PDstandardNthfdOrder823(&gt22[index]);
        PDstandardNth1gt23 = PDstandardNthfdOrder81(&gt23[index]);
        PDstandardNth2gt23 = PDstandardNthfdOrder82(&gt23[index]);
        PDstandardNth3gt23 = PDstandardNthfdOrder83(&gt23[index]);
        PDstandardNth11gt23 = PDstandardNthfdOrder811(&gt23[index]);
        PDstandardNth22gt23 = PDstandardNthfdOrder822(&gt23[index]);
        PDstandardNth33gt23 = PDstandardNthfdOrder833(&gt23[index]);
        PDstandardNth12gt23 = PDstandardNthfdOrder812(&gt23[index]);
        PDstandardNth13gt23 = PDstandardNthfdOrder813(&gt23[index]);
        PDstandardNth23gt23 = PDstandardNthfdOrder823(&gt23[index]);
        PDstandardNth1gt33 = PDstandardNthfdOrder81(&gt33[index]);
        PDstandardNth2gt33 = PDstandardNthfdOrder82(&gt33[index]);
        PDstandardNth3gt33 = PDstandardNthfdOrder83(&gt33[index]);
        PDstandardNth11gt33 = PDstandardNthfdOrder811(&gt33[index]);
        PDstandardNth22gt33 = PDstandardNthfdOrder822(&gt33[index]);
        PDstandardNth33gt33 = PDstandardNthfdOrder833(&gt33[index]);
        PDstandardNth12gt33 = PDstandardNthfdOrder812(&gt33[index]);
        PDstandardNth13gt33 = PDstandardNthfdOrder813(&gt33[index]);
        PDstandardNth23gt33 = PDstandardNthfdOrder823(&gt33[index]);
        PDstandardNth1phi = PDstandardNthfdOrder81(&phi[index]);
        PDstandardNth2phi = PDstandardNthfdOrder82(&phi[index]);
        PDstandardNth3phi = PDstandardNthfdOrder83(&phi[index]);
        PDstandardNth11phi = PDstandardNthfdOrder811(&phi[index]);
        PDstandardNth22phi = PDstandardNthfdOrder822(&phi[index]);
        PDstandardNth33phi = PDstandardNthfdOrder833(&phi[index]);
        PDstandardNth12phi = PDstandardNthfdOrder812(&phi[index]);
        PDstandardNth13phi = PDstandardNthfdOrder813(&phi[index]);
        PDstandardNth23phi = PDstandardNthfdOrder823(&phi[index]);
        PDstandardNth1trK = PDstandardNthfdOrder81(&trK[index]);
        PDstandardNth2trK = PDstandardNthfdOrder82(&trK[index]);
        PDstandardNth3trK = PDstandardNthfdOrder83(&trK[index]);
        break;
      }
      default:
        CCTK_BUILTIN_UNREACHABLE();
    }
    
    /* Calculate temporaries and grid functions */
    CCTK_REAL_VEC JacPDstandardNth11gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth11gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth11gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth11phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth12gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth12gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth12gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth12gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth12phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth13gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth13gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth13gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth13gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth13phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1At11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1At12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1At13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1At22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1At23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1At33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth1trK CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth22gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth22gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth22gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth22phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth23gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth23gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth23gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth23gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth23phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2At11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2At12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2At13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2At22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2At23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2At33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth2trK CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth33gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth33gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth33gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth33phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3At11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3At12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3At13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3At22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3At23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3At33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3gt11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3gt12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3gt13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3gt22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3gt23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3gt33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3phi CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC JacPDstandardNth3trK CCTK_ATTRIBUTE_UNUSED;
    
    if (use_jacobian)
    {
      JacPDstandardNth1At11 = 
        kmadd(J11L,PDstandardNth1At11,kmadd(J21L,PDstandardNth2At11,kmul(J31L,PDstandardNth3At11)));
      
      JacPDstandardNth1At12 = 
        kmadd(J11L,PDstandardNth1At12,kmadd(J21L,PDstandardNth2At12,kmul(J31L,PDstandardNth3At12)));
      
      JacPDstandardNth1At13 = 
        kmadd(J11L,PDstandardNth1At13,kmadd(J21L,PDstandardNth2At13,kmul(J31L,PDstandardNth3At13)));
      
      JacPDstandardNth1At22 = 
        kmadd(J11L,PDstandardNth1At22,kmadd(J21L,PDstandardNth2At22,kmul(J31L,PDstandardNth3At22)));
      
      JacPDstandardNth1At23 = 
        kmadd(J11L,PDstandardNth1At23,kmadd(J21L,PDstandardNth2At23,kmul(J31L,PDstandardNth3At23)));
      
      JacPDstandardNth1At33 = 
        kmadd(J11L,PDstandardNth1At33,kmadd(J21L,PDstandardNth2At33,kmul(J31L,PDstandardNth3At33)));
      
      JacPDstandardNth1gt11 = 
        kmadd(J11L,PDstandardNth1gt11,kmadd(J21L,PDstandardNth2gt11,kmul(J31L,PDstandardNth3gt11)));
      
      JacPDstandardNth1gt12 = 
        kmadd(J11L,PDstandardNth1gt12,kmadd(J21L,PDstandardNth2gt12,kmul(J31L,PDstandardNth3gt12)));
      
      JacPDstandardNth1gt13 = 
        kmadd(J11L,PDstandardNth1gt13,kmadd(J21L,PDstandardNth2gt13,kmul(J31L,PDstandardNth3gt13)));
      
      JacPDstandardNth1gt22 = 
        kmadd(J11L,PDstandardNth1gt22,kmadd(J21L,PDstandardNth2gt22,kmul(J31L,PDstandardNth3gt22)));
      
      JacPDstandardNth1gt23 = 
        kmadd(J11L,PDstandardNth1gt23,kmadd(J21L,PDstandardNth2gt23,kmul(J31L,PDstandardNth3gt23)));
      
      JacPDstandardNth1gt33 = 
        kmadd(J11L,PDstandardNth1gt33,kmadd(J21L,PDstandardNth2gt33,kmul(J31L,PDstandardNth3gt33)));
      
      JacPDstandardNth1phi = 
        kmadd(J11L,PDstandardNth1phi,kmadd(J21L,PDstandardNth2phi,kmul(J31L,PDstandardNth3phi)));
      
      JacPDstandardNth1trK = 
        kmadd(J11L,PDstandardNth1trK,kmadd(J21L,PDstandardNth2trK,kmul(J31L,PDstandardNth3trK)));
      
      JacPDstandardNth2At11 = 
        kmadd(J12L,PDstandardNth1At11,kmadd(J22L,PDstandardNth2At11,kmul(J32L,PDstandardNth3At11)));
      
      JacPDstandardNth2At12 = 
        kmadd(J12L,PDstandardNth1At12,kmadd(J22L,PDstandardNth2At12,kmul(J32L,PDstandardNth3At12)));
      
      JacPDstandardNth2At13 = 
        kmadd(J12L,PDstandardNth1At13,kmadd(J22L,PDstandardNth2At13,kmul(J32L,PDstandardNth3At13)));
      
      JacPDstandardNth2At22 = 
        kmadd(J12L,PDstandardNth1At22,kmadd(J22L,PDstandardNth2At22,kmul(J32L,PDstandardNth3At22)));
      
      JacPDstandardNth2At23 = 
        kmadd(J12L,PDstandardNth1At23,kmadd(J22L,PDstandardNth2At23,kmul(J32L,PDstandardNth3At23)));
      
      JacPDstandardNth2At33 = 
        kmadd(J12L,PDstandardNth1At33,kmadd(J22L,PDstandardNth2At33,kmul(J32L,PDstandardNth3At33)));
      
      JacPDstandardNth2gt11 = 
        kmadd(J12L,PDstandardNth1gt11,kmadd(J22L,PDstandardNth2gt11,kmul(J32L,PDstandardNth3gt11)));
      
      JacPDstandardNth2gt12 = 
        kmadd(J12L,PDstandardNth1gt12,kmadd(J22L,PDstandardNth2gt12,kmul(J32L,PDstandardNth3gt12)));
      
      JacPDstandardNth2gt13 = 
        kmadd(J12L,PDstandardNth1gt13,kmadd(J22L,PDstandardNth2gt13,kmul(J32L,PDstandardNth3gt13)));
      
      JacPDstandardNth2gt22 = 
        kmadd(J12L,PDstandardNth1gt22,kmadd(J22L,PDstandardNth2gt22,kmul(J32L,PDstandardNth3gt22)));
      
      JacPDstandardNth2gt23 = 
        kmadd(J12L,PDstandardNth1gt23,kmadd(J22L,PDstandardNth2gt23,kmul(J32L,PDstandardNth3gt23)));
      
      JacPDstandardNth2gt33 = 
        kmadd(J12L,PDstandardNth1gt33,kmadd(J22L,PDstandardNth2gt33,kmul(J32L,PDstandardNth3gt33)));
      
      JacPDstandardNth2phi = 
        kmadd(J12L,PDstandardNth1phi,kmadd(J22L,PDstandardNth2phi,kmul(J32L,PDstandardNth3phi)));
      
      JacPDstandardNth2trK = 
        kmadd(J12L,PDstandardNth1trK,kmadd(J22L,PDstandardNth2trK,kmul(J32L,PDstandardNth3trK)));
      
      JacPDstandardNth3At11 = 
        kmadd(J13L,PDstandardNth1At11,kmadd(J23L,PDstandardNth2At11,kmul(J33L,PDstandardNth3At11)));
      
      JacPDstandardNth3At12 = 
        kmadd(J13L,PDstandardNth1At12,kmadd(J23L,PDstandardNth2At12,kmul(J33L,PDstandardNth3At12)));
      
      JacPDstandardNth3At13 = 
        kmadd(J13L,PDstandardNth1At13,kmadd(J23L,PDstandardNth2At13,kmul(J33L,PDstandardNth3At13)));
      
      JacPDstandardNth3At22 = 
        kmadd(J13L,PDstandardNth1At22,kmadd(J23L,PDstandardNth2At22,kmul(J33L,PDstandardNth3At22)));
      
      JacPDstandardNth3At23 = 
        kmadd(J13L,PDstandardNth1At23,kmadd(J23L,PDstandardNth2At23,kmul(J33L,PDstandardNth3At23)));
      
      JacPDstandardNth3At33 = 
        kmadd(J13L,PDstandardNth1At33,kmadd(J23L,PDstandardNth2At33,kmul(J33L,PDstandardNth3At33)));
      
      JacPDstandardNth3gt11 = 
        kmadd(J13L,PDstandardNth1gt11,kmadd(J23L,PDstandardNth2gt11,kmul(J33L,PDstandardNth3gt11)));
      
      JacPDstandardNth3gt12 = 
        kmadd(J13L,PDstandardNth1gt12,kmadd(J23L,PDstandardNth2gt12,kmul(J33L,PDstandardNth3gt12)));
      
      JacPDstandardNth3gt13 = 
        kmadd(J13L,PDstandardNth1gt13,kmadd(J23L,PDstandardNth2gt13,kmul(J33L,PDstandardNth3gt13)));
      
      JacPDstandardNth3gt22 = 
        kmadd(J13L,PDstandardNth1gt22,kmadd(J23L,PDstandardNth2gt22,kmul(J33L,PDstandardNth3gt22)));
      
      JacPDstandardNth3gt23 = 
        kmadd(J13L,PDstandardNth1gt23,kmadd(J23L,PDstandardNth2gt23,kmul(J33L,PDstandardNth3gt23)));
      
      JacPDstandardNth3gt33 = 
        kmadd(J13L,PDstandardNth1gt33,kmadd(J23L,PDstandardNth2gt33,kmul(J33L,PDstandardNth3gt33)));
      
      JacPDstandardNth3phi = 
        kmadd(J13L,PDstandardNth1phi,kmadd(J23L,PDstandardNth2phi,kmul(J33L,PDstandardNth3phi)));
      
      JacPDstandardNth3trK = 
        kmadd(J13L,PDstandardNth1trK,kmadd(J23L,PDstandardNth2trK,kmul(J33L,PDstandardNth3trK)));
      
      JacPDstandardNth11gt22 = 
        kmadd(dJ111L,PDstandardNth1gt22,kmadd(dJ211L,PDstandardNth2gt22,kmadd(dJ311L,PDstandardNth3gt22,kmadd(PDstandardNth11gt22,kmul(J11L,J11L),kmadd(PDstandardNth22gt22,kmul(J21L,J21L),kmadd(PDstandardNth33gt22,kmul(J31L,J31L),kmul(kmadd(J11L,kmadd(J21L,PDstandardNth12gt22,kmul(J31L,PDstandardNth13gt22)),kmul(J21L,kmul(J31L,PDstandardNth23gt22))),ToReal(2))))))));
      
      JacPDstandardNth11gt23 = 
        kmadd(dJ111L,PDstandardNth1gt23,kmadd(dJ211L,PDstandardNth2gt23,kmadd(dJ311L,PDstandardNth3gt23,kmadd(PDstandardNth11gt23,kmul(J11L,J11L),kmadd(PDstandardNth22gt23,kmul(J21L,J21L),kmadd(PDstandardNth33gt23,kmul(J31L,J31L),kmul(kmadd(J11L,kmadd(J21L,PDstandardNth12gt23,kmul(J31L,PDstandardNth13gt23)),kmul(J21L,kmul(J31L,PDstandardNth23gt23))),ToReal(2))))))));
      
      JacPDstandardNth11gt33 = 
        kmadd(dJ111L,PDstandardNth1gt33,kmadd(dJ211L,PDstandardNth2gt33,kmadd(dJ311L,PDstandardNth3gt33,kmadd(PDstandardNth11gt33,kmul(J11L,J11L),kmadd(PDstandardNth22gt33,kmul(J21L,J21L),kmadd(PDstandardNth33gt33,kmul(J31L,J31L),kmul(kmadd(J11L,kmadd(J21L,PDstandardNth12gt33,kmul(J31L,PDstandardNth13gt33)),kmul(J21L,kmul(J31L,PDstandardNth23gt33))),ToReal(2))))))));
      
      JacPDstandardNth11phi = 
        kmadd(dJ111L,PDstandardNth1phi,kmadd(dJ211L,PDstandardNth2phi,kmadd(dJ311L,PDstandardNth3phi,kmadd(PDstandardNth11phi,kmul(J11L,J11L),kmadd(PDstandardNth22phi,kmul(J21L,J21L),kmadd(PDstandardNth33phi,kmul(J31L,J31L),kmul(kmadd(J11L,kmadd(J21L,PDstandardNth12phi,kmul(J31L,PDstandardNth13phi)),kmul(J21L,kmul(J31L,PDstandardNth23phi))),ToReal(2))))))));
      
      JacPDstandardNth22gt11 = 
        kmadd(dJ122L,PDstandardNth1gt11,kmadd(dJ222L,PDstandardNth2gt11,kmadd(dJ322L,PDstandardNth3gt11,kmadd(PDstandardNth11gt11,kmul(J12L,J12L),kmadd(PDstandardNth22gt11,kmul(J22L,J22L),kmadd(PDstandardNth33gt11,kmul(J32L,J32L),kmul(kmadd(J12L,kmadd(J22L,PDstandardNth12gt11,kmul(J32L,PDstandardNth13gt11)),kmul(J22L,kmul(J32L,PDstandardNth23gt11))),ToReal(2))))))));
      
      JacPDstandardNth22gt13 = 
        kmadd(dJ122L,PDstandardNth1gt13,kmadd(dJ222L,PDstandardNth2gt13,kmadd(dJ322L,PDstandardNth3gt13,kmadd(PDstandardNth11gt13,kmul(J12L,J12L),kmadd(PDstandardNth22gt13,kmul(J22L,J22L),kmadd(PDstandardNth33gt13,kmul(J32L,J32L),kmul(kmadd(J12L,kmadd(J22L,PDstandardNth12gt13,kmul(J32L,PDstandardNth13gt13)),kmul(J22L,kmul(J32L,PDstandardNth23gt13))),ToReal(2))))))));
      
      JacPDstandardNth22gt33 = 
        kmadd(dJ122L,PDstandardNth1gt33,kmadd(dJ222L,PDstandardNth2gt33,kmadd(dJ322L,PDstandardNth3gt33,kmadd(PDstandardNth11gt33,kmul(J12L,J12L),kmadd(PDstandardNth22gt33,kmul(J22L,J22L),kmadd(PDstandardNth33gt33,kmul(J32L,J32L),kmul(kmadd(J12L,kmadd(J22L,PDstandardNth12gt33,kmul(J32L,PDstandardNth13gt33)),kmul(J22L,kmul(J32L,PDstandardNth23gt33))),ToReal(2))))))));
      
      JacPDstandardNth22phi = 
        kmadd(dJ122L,PDstandardNth1phi,kmadd(dJ222L,PDstandardNth2phi,kmadd(dJ322L,PDstandardNth3phi,kmadd(PDstandardNth11phi,kmul(J12L,J12L),kmadd(PDstandardNth22phi,kmul(J22L,J22L),kmadd(PDstandardNth33phi,kmul(J32L,J32L),kmul(kmadd(J12L,kmadd(J22L,PDstandardNth12phi,kmul(J32L,PDstandardNth13phi)),kmul(J22L,kmul(J32L,PDstandardNth23phi))),ToReal(2))))))));
      
      JacPDstandardNth33gt11 = 
        kmadd(dJ133L,PDstandardNth1gt11,kmadd(dJ233L,PDstandardNth2gt11,kmadd(dJ333L,PDstandardNth3gt11,kmadd(PDstandardNth11gt11,kmul(J13L,J13L),kmadd(PDstandardNth22gt11,kmul(J23L,J23L),kmadd(PDstandardNth33gt11,kmul(J33L,J33L),kmul(kmadd(J13L,kmadd(J23L,PDstandardNth12gt11,kmul(J33L,PDstandardNth13gt11)),kmul(J23L,kmul(J33L,PDstandardNth23gt11))),ToReal(2))))))));
      
      JacPDstandardNth33gt12 = 
        kmadd(dJ133L,PDstandardNth1gt12,kmadd(dJ233L,PDstandardNth2gt12,kmadd(dJ333L,PDstandardNth3gt12,kmadd(PDstandardNth11gt12,kmul(J13L,J13L),kmadd(PDstandardNth22gt12,kmul(J23L,J23L),kmadd(PDstandardNth33gt12,kmul(J33L,J33L),kmul(kmadd(J13L,kmadd(J23L,PDstandardNth12gt12,kmul(J33L,PDstandardNth13gt12)),kmul(J23L,kmul(J33L,PDstandardNth23gt12))),ToReal(2))))))));
      
      JacPDstandardNth33gt22 = 
        kmadd(dJ133L,PDstandardNth1gt22,kmadd(dJ233L,PDstandardNth2gt22,kmadd(dJ333L,PDstandardNth3gt22,kmadd(PDstandardNth11gt22,kmul(J13L,J13L),kmadd(PDstandardNth22gt22,kmul(J23L,J23L),kmadd(PDstandardNth33gt22,kmul(J33L,J33L),kmul(kmadd(J13L,kmadd(J23L,PDstandardNth12gt22,kmul(J33L,PDstandardNth13gt22)),kmul(J23L,kmul(J33L,PDstandardNth23gt22))),ToReal(2))))))));
      
      JacPDstandardNth33phi = 
        kmadd(dJ133L,PDstandardNth1phi,kmadd(dJ233L,PDstandardNth2phi,kmadd(dJ333L,PDstandardNth3phi,kmadd(PDstandardNth11phi,kmul(J13L,J13L),kmadd(PDstandardNth22phi,kmul(J23L,J23L),kmadd(PDstandardNth33phi,kmul(J33L,J33L),kmul(kmadd(J13L,kmadd(J23L,PDstandardNth12phi,kmul(J33L,PDstandardNth13phi)),kmul(J23L,kmul(J33L,PDstandardNth23phi))),ToReal(2))))))));
      
      JacPDstandardNth12gt12 = 
        kmadd(J12L,kmadd(J11L,PDstandardNth11gt12,kmadd(J21L,PDstandardNth12gt12,kmul(J31L,PDstandardNth13gt12))),kmadd(J11L,kmadd(J22L,PDstandardNth12gt12,kmul(J32L,PDstandardNth13gt12)),kmadd(dJ112L,PDstandardNth1gt12,kmadd(J22L,kmadd(J21L,PDstandardNth22gt12,kmul(J31L,PDstandardNth23gt12)),kmadd(dJ212L,PDstandardNth2gt12,kmadd(J32L,kmadd(J21L,PDstandardNth23gt12,kmul(J31L,PDstandardNth33gt12)),kmul(dJ312L,PDstandardNth3gt12)))))));
      
      JacPDstandardNth12gt13 = 
        kmadd(J12L,kmadd(J11L,PDstandardNth11gt13,kmadd(J21L,PDstandardNth12gt13,kmul(J31L,PDstandardNth13gt13))),kmadd(J11L,kmadd(J22L,PDstandardNth12gt13,kmul(J32L,PDstandardNth13gt13)),kmadd(dJ112L,PDstandardNth1gt13,kmadd(J22L,kmadd(J21L,PDstandardNth22gt13,kmul(J31L,PDstandardNth23gt13)),kmadd(dJ212L,PDstandardNth2gt13,kmadd(J32L,kmadd(J21L,PDstandardNth23gt13,kmul(J31L,PDstandardNth33gt13)),kmul(dJ312L,PDstandardNth3gt13)))))));
      
      JacPDstandardNth12gt23 = 
        kmadd(J12L,kmadd(J11L,PDstandardNth11gt23,kmadd(J21L,PDstandardNth12gt23,kmul(J31L,PDstandardNth13gt23))),kmadd(J11L,kmadd(J22L,PDstandardNth12gt23,kmul(J32L,PDstandardNth13gt23)),kmadd(dJ112L,PDstandardNth1gt23,kmadd(J22L,kmadd(J21L,PDstandardNth22gt23,kmul(J31L,PDstandardNth23gt23)),kmadd(dJ212L,PDstandardNth2gt23,kmadd(J32L,kmadd(J21L,PDstandardNth23gt23,kmul(J31L,PDstandardNth33gt23)),kmul(dJ312L,PDstandardNth3gt23)))))));
      
      JacPDstandardNth12gt33 = 
        kmadd(J12L,kmadd(J11L,PDstandardNth11gt33,kmadd(J21L,PDstandardNth12gt33,kmul(J31L,PDstandardNth13gt33))),kmadd(J11L,kmadd(J22L,PDstandardNth12gt33,kmul(J32L,PDstandardNth13gt33)),kmadd(dJ112L,PDstandardNth1gt33,kmadd(J22L,kmadd(J21L,PDstandardNth22gt33,kmul(J31L,PDstandardNth23gt33)),kmadd(dJ212L,PDstandardNth2gt33,kmadd(J32L,kmadd(J21L,PDstandardNth23gt33,kmul(J31L,PDstandardNth33gt33)),kmul(dJ312L,PDstandardNth3gt33)))))));
      
      JacPDstandardNth12phi = 
        kmadd(J12L,kmadd(J11L,PDstandardNth11phi,kmadd(J21L,PDstandardNth12phi,kmul(J31L,PDstandardNth13phi))),kmadd(J11L,kmadd(J22L,PDstandardNth12phi,kmul(J32L,PDstandardNth13phi)),kmadd(dJ112L,PDstandardNth1phi,kmadd(J22L,kmadd(J21L,PDstandardNth22phi,kmul(J31L,PDstandardNth23phi)),kmadd(dJ212L,PDstandardNth2phi,kmadd(J32L,kmadd(J21L,PDstandardNth23phi,kmul(J31L,PDstandardNth33phi)),kmul(dJ312L,PDstandardNth3phi)))))));
      
      JacPDstandardNth13gt12 = 
        kmadd(J13L,kmadd(J11L,PDstandardNth11gt12,kmadd(J21L,PDstandardNth12gt12,kmul(J31L,PDstandardNth13gt12))),kmadd(J11L,kmadd(J23L,PDstandardNth12gt12,kmul(J33L,PDstandardNth13gt12)),kmadd(dJ113L,PDstandardNth1gt12,kmadd(J23L,kmadd(J21L,PDstandardNth22gt12,kmul(J31L,PDstandardNth23gt12)),kmadd(dJ213L,PDstandardNth2gt12,kmadd(J33L,kmadd(J21L,PDstandardNth23gt12,kmul(J31L,PDstandardNth33gt12)),kmul(dJ313L,PDstandardNth3gt12)))))));
      
      JacPDstandardNth13gt13 = 
        kmadd(J13L,kmadd(J11L,PDstandardNth11gt13,kmadd(J21L,PDstandardNth12gt13,kmul(J31L,PDstandardNth13gt13))),kmadd(J11L,kmadd(J23L,PDstandardNth12gt13,kmul(J33L,PDstandardNth13gt13)),kmadd(dJ113L,PDstandardNth1gt13,kmadd(J23L,kmadd(J21L,PDstandardNth22gt13,kmul(J31L,PDstandardNth23gt13)),kmadd(dJ213L,PDstandardNth2gt13,kmadd(J33L,kmadd(J21L,PDstandardNth23gt13,kmul(J31L,PDstandardNth33gt13)),kmul(dJ313L,PDstandardNth3gt13)))))));
      
      JacPDstandardNth13gt22 = 
        kmadd(J13L,kmadd(J11L,PDstandardNth11gt22,kmadd(J21L,PDstandardNth12gt22,kmul(J31L,PDstandardNth13gt22))),kmadd(J11L,kmadd(J23L,PDstandardNth12gt22,kmul(J33L,PDstandardNth13gt22)),kmadd(dJ113L,PDstandardNth1gt22,kmadd(J23L,kmadd(J21L,PDstandardNth22gt22,kmul(J31L,PDstandardNth23gt22)),kmadd(dJ213L,PDstandardNth2gt22,kmadd(J33L,kmadd(J21L,PDstandardNth23gt22,kmul(J31L,PDstandardNth33gt22)),kmul(dJ313L,PDstandardNth3gt22)))))));
      
      JacPDstandardNth13gt23 = 
        kmadd(J13L,kmadd(J11L,PDstandardNth11gt23,kmadd(J21L,PDstandardNth12gt23,kmul(J31L,PDstandardNth13gt23))),kmadd(J11L,kmadd(J23L,PDstandardNth12gt23,kmul(J33L,PDstandardNth13gt23)),kmadd(dJ113L,PDstandardNth1gt23,kmadd(J23L,kmadd(J21L,PDstandardNth22gt23,kmul(J31L,PDstandardNth23gt23)),kmadd(dJ213L,PDstandardNth2gt23,kmadd(J33L,kmadd(J21L,PDstandardNth23gt23,kmul(J31L,PDstandardNth33gt23)),kmul(dJ313L,PDstandardNth3gt23)))))));
      
      JacPDstandardNth13phi = 
        kmadd(J13L,kmadd(J11L,PDstandardNth11phi,kmadd(J21L,PDstandardNth12phi,kmul(J31L,PDstandardNth13phi))),kmadd(J11L,kmadd(J23L,PDstandardNth12phi,kmul(J33L,PDstandardNth13phi)),kmadd(dJ113L,PDstandardNth1phi,kmadd(J23L,kmadd(J21L,PDstandardNth22phi,kmul(J31L,PDstandardNth23phi)),kmadd(dJ213L,PDstandardNth2phi,kmadd(J33L,kmadd(J21L,PDstandardNth23phi,kmul(J31L,PDstandardNth33phi)),kmul(dJ313L,PDstandardNth3phi)))))));
      
      JacPDstandardNth23gt11 = 
        kmadd(J13L,kmadd(J12L,PDstandardNth11gt11,kmadd(J22L,PDstandardNth12gt11,kmul(J32L,PDstandardNth13gt11))),kmadd(J12L,kmadd(J23L,PDstandardNth12gt11,kmul(J33L,PDstandardNth13gt11)),kmadd(dJ123L,PDstandardNth1gt11,kmadd(J23L,kmadd(J22L,PDstandardNth22gt11,kmul(J32L,PDstandardNth23gt11)),kmadd(dJ223L,PDstandardNth2gt11,kmadd(J33L,kmadd(J22L,PDstandardNth23gt11,kmul(J32L,PDstandardNth33gt11)),kmul(dJ323L,PDstandardNth3gt11)))))));
      
      JacPDstandardNth23gt12 = 
        kmadd(J13L,kmadd(J12L,PDstandardNth11gt12,kmadd(J22L,PDstandardNth12gt12,kmul(J32L,PDstandardNth13gt12))),kmadd(J12L,kmadd(J23L,PDstandardNth12gt12,kmul(J33L,PDstandardNth13gt12)),kmadd(dJ123L,PDstandardNth1gt12,kmadd(J23L,kmadd(J22L,PDstandardNth22gt12,kmul(J32L,PDstandardNth23gt12)),kmadd(dJ223L,PDstandardNth2gt12,kmadd(J33L,kmadd(J22L,PDstandardNth23gt12,kmul(J32L,PDstandardNth33gt12)),kmul(dJ323L,PDstandardNth3gt12)))))));
      
      JacPDstandardNth23gt13 = 
        kmadd(J13L,kmadd(J12L,PDstandardNth11gt13,kmadd(J22L,PDstandardNth12gt13,kmul(J32L,PDstandardNth13gt13))),kmadd(J12L,kmadd(J23L,PDstandardNth12gt13,kmul(J33L,PDstandardNth13gt13)),kmadd(dJ123L,PDstandardNth1gt13,kmadd(J23L,kmadd(J22L,PDstandardNth22gt13,kmul(J32L,PDstandardNth23gt13)),kmadd(dJ223L,PDstandardNth2gt13,kmadd(J33L,kmadd(J22L,PDstandardNth23gt13,kmul(J32L,PDstandardNth33gt13)),kmul(dJ323L,PDstandardNth3gt13)))))));
      
      JacPDstandardNth23gt23 = 
        kmadd(J13L,kmadd(J12L,PDstandardNth11gt23,kmadd(J22L,PDstandardNth12gt23,kmul(J32L,PDstandardNth13gt23))),kmadd(J12L,kmadd(J23L,PDstandardNth12gt23,kmul(J33L,PDstandardNth13gt23)),kmadd(dJ123L,PDstandardNth1gt23,kmadd(J23L,kmadd(J22L,PDstandardNth22gt23,kmul(J32L,PDstandardNth23gt23)),kmadd(dJ223L,PDstandardNth2gt23,kmadd(J33L,kmadd(J22L,PDstandardNth23gt23,kmul(J32L,PDstandardNth33gt23)),kmul(dJ323L,PDstandardNth3gt23)))))));
      
      JacPDstandardNth23phi = 
        kmadd(J13L,kmadd(J12L,PDstandardNth11phi,kmadd(J22L,PDstandardNth12phi,kmul(J32L,PDstandardNth13phi))),kmadd(J12L,kmadd(J23L,PDstandardNth12phi,kmul(J33L,PDstandardNth13phi)),kmadd(dJ123L,PDstandardNth1phi,kmadd(J23L,kmadd(J22L,PDstandardNth22phi,kmul(J32L,PDstandardNth23phi)),kmadd(dJ223L,PDstandardNth2phi,kmadd(J33L,kmadd(J22L,PDstandardNth23phi,kmul(J32L,PDstandardNth33phi)),kmul(dJ323L,PDstandardNth3phi)))))));
    }
    else
    {
      JacPDstandardNth1At11 = PDstandardNth1At11;
      
      JacPDstandardNth1At12 = PDstandardNth1At12;
      
      JacPDstandardNth1At13 = PDstandardNth1At13;
      
      JacPDstandardNth1At22 = PDstandardNth1At22;
      
      JacPDstandardNth1At23 = PDstandardNth1At23;
      
      JacPDstandardNth1At33 = PDstandardNth1At33;
      
      JacPDstandardNth1gt11 = PDstandardNth1gt11;
      
      JacPDstandardNth1gt12 = PDstandardNth1gt12;
      
      JacPDstandardNth1gt13 = PDstandardNth1gt13;
      
      JacPDstandardNth1gt22 = PDstandardNth1gt22;
      
      JacPDstandardNth1gt23 = PDstandardNth1gt23;
      
      JacPDstandardNth1gt33 = PDstandardNth1gt33;
      
      JacPDstandardNth1phi = PDstandardNth1phi;
      
      JacPDstandardNth1trK = PDstandardNth1trK;
      
      JacPDstandardNth2At11 = PDstandardNth2At11;
      
      JacPDstandardNth2At12 = PDstandardNth2At12;
      
      JacPDstandardNth2At13 = PDstandardNth2At13;
      
      JacPDstandardNth2At22 = PDstandardNth2At22;
      
      JacPDstandardNth2At23 = PDstandardNth2At23;
      
      JacPDstandardNth2At33 = PDstandardNth2At33;
      
      JacPDstandardNth2gt11 = PDstandardNth2gt11;
      
      JacPDstandardNth2gt12 = PDstandardNth2gt12;
      
      JacPDstandardNth2gt13 = PDstandardNth2gt13;
      
      JacPDstandardNth2gt22 = PDstandardNth2gt22;
      
      JacPDstandardNth2gt23 = PDstandardNth2gt23;
      
      JacPDstandardNth2gt33 = PDstandardNth2gt33;
      
      JacPDstandardNth2phi = PDstandardNth2phi;
      
      JacPDstandardNth2trK = PDstandardNth2trK;
      
      JacPDstandardNth3At11 = PDstandardNth3At11;
      
      JacPDstandardNth3At12 = PDstandardNth3At12;
      
      JacPDstandardNth3At13 = PDstandardNth3At13;
      
      JacPDstandardNth3At22 = PDstandardNth3At22;
      
      JacPDstandardNth3At23 = PDstandardNth3At23;
      
      JacPDstandardNth3At33 = PDstandardNth3At33;
      
      JacPDstandardNth3gt11 = PDstandardNth3gt11;
      
      JacPDstandardNth3gt12 = PDstandardNth3gt12;
      
      JacPDstandardNth3gt13 = PDstandardNth3gt13;
      
      JacPDstandardNth3gt22 = PDstandardNth3gt22;
      
      JacPDstandardNth3gt23 = PDstandardNth3gt23;
      
      JacPDstandardNth3gt33 = PDstandardNth3gt33;
      
      JacPDstandardNth3phi = PDstandardNth3phi;
      
      JacPDstandardNth3trK = PDstandardNth3trK;
      
      JacPDstandardNth11gt22 = PDstandardNth11gt22;
      
      JacPDstandardNth11gt23 = PDstandardNth11gt23;
      
      JacPDstandardNth11gt33 = PDstandardNth11gt33;
      
      JacPDstandardNth11phi = PDstandardNth11phi;
      
      JacPDstandardNth22gt11 = PDstandardNth22gt11;
      
      JacPDstandardNth22gt13 = PDstandardNth22gt13;
      
      JacPDstandardNth22gt33 = PDstandardNth22gt33;
      
      JacPDstandardNth22phi = PDstandardNth22phi;
      
      JacPDstandardNth33gt11 = PDstandardNth33gt11;
      
      JacPDstandardNth33gt12 = PDstandardNth33gt12;
      
      JacPDstandardNth33gt22 = PDstandardNth33gt22;
      
      JacPDstandardNth33phi = PDstandardNth33phi;
      
      JacPDstandardNth12gt12 = PDstandardNth12gt12;
      
      JacPDstandardNth12gt13 = PDstandardNth12gt13;
      
      JacPDstandardNth12gt23 = PDstandardNth12gt23;
      
      JacPDstandardNth12gt33 = PDstandardNth12gt33;
      
      JacPDstandardNth12phi = PDstandardNth12phi;
      
      JacPDstandardNth13gt12 = PDstandardNth13gt12;
      
      JacPDstandardNth13gt13 = PDstandardNth13gt13;
      
      JacPDstandardNth13gt22 = PDstandardNth13gt22;
      
      JacPDstandardNth13gt23 = PDstandardNth13gt23;
      
      JacPDstandardNth13phi = PDstandardNth13phi;
      
      JacPDstandardNth23gt11 = PDstandardNth23gt11;
      
      JacPDstandardNth23gt12 = PDstandardNth23gt12;
      
      JacPDstandardNth23gt13 = PDstandardNth23gt13;
      
      JacPDstandardNth23gt23 = PDstandardNth23gt23;
      
      JacPDstandardNth23phi = PDstandardNth23phi;
    }
    
    CCTK_REAL_VEC D2phi11 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC D2phi12 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC D2phi13 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC D2phi22 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC D2phi23 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC D2phi33 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC Dphi1 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC Dphi2 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC Dphi3 CCTK_ATTRIBUTE_UNUSED;
    CCTK_REAL_VEC e4phi CCTK_ATTRIBUTE_UNUSED;
    
    if (conformalMethod == 1)
    {
      e4phi = kdiv(ToReal(1),kmul(phiL,phiL));
      
      Dphi1 = kdiv(kmul(JacPDstandardNth1phi,ToReal(-0.5)),phiL);
      
      Dphi2 = kdiv(kmul(JacPDstandardNth2phi,ToReal(-0.5)),phiL);
      
      Dphi3 = kdiv(kmul(JacPDstandardNth3phi,ToReal(-0.5)),phiL);
      
      D2phi11 = 
        kdiv(kmul(kmsub(JacPDstandardNth1phi,JacPDstandardNth1phi,kmul(phiL,JacPDstandardNth11phi)),ToReal(0.5)),kmul(phiL,phiL));
      
      D2phi12 = 
        kdiv(kmul(kmsub(JacPDstandardNth1phi,JacPDstandardNth2phi,kmul(phiL,JacPDstandardNth12phi)),ToReal(0.5)),kmul(phiL,phiL));
      
      D2phi13 = 
        kdiv(kmul(kmsub(JacPDstandardNth1phi,JacPDstandardNth3phi,kmul(phiL,JacPDstandardNth13phi)),ToReal(0.5)),kmul(phiL,phiL));
      
      D2phi22 = 
        kdiv(kmul(kmsub(JacPDstandardNth2phi,JacPDstandardNth2phi,kmul(phiL,JacPDstandardNth22phi)),ToReal(0.5)),kmul(phiL,phiL));
      
      D2phi23 = 
        kdiv(kmul(kmsub(JacPDstandardNth2phi,JacPDstandardNth3phi,kmul(phiL,JacPDstandardNth23phi)),ToReal(0.5)),kmul(phiL,phiL));
      
      D2phi33 = 
        kdiv(kmul(kmsub(JacPDstandardNth3phi,JacPDstandardNth3phi,kmul(phiL,JacPDstandardNth33phi)),ToReal(0.5)),kmul(phiL,phiL));
    }
    else
    {
      e4phi = kexp(kmul(phiL,ToReal(4)));
      
      Dphi1 = JacPDstandardNth1phi;
      
      Dphi2 = JacPDstandardNth2phi;
      
      Dphi3 = JacPDstandardNth3phi;
      
      D2phi11 = JacPDstandardNth11phi;
      
      D2phi12 = JacPDstandardNth12phi;
      
      D2phi13 = JacPDstandardNth13phi;
      
      D2phi22 = JacPDstandardNth22phi;
      
      D2phi23 = JacPDstandardNth23phi;
      
      D2phi33 = JacPDstandardNth33phi;
    }
    
    CCTK_REAL_VEC Dg111 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt11L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1gt11));
    
    CCTK_REAL_VEC Dg112 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt11L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt11));
    
    CCTK_REAL_VEC Dg113 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt11L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt11));
    
    CCTK_REAL_VEC Dg121 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt12L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1gt12));
    
    CCTK_REAL_VEC Dg122 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt12L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt12));
    
    CCTK_REAL_VEC Dg123 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt12L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt12));
    
    CCTK_REAL_VEC Dg131 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt13L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1gt13));
    
    CCTK_REAL_VEC Dg132 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt13L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt13));
    
    CCTK_REAL_VEC Dg133 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt13L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt13));
    
    CCTK_REAL_VEC Dg221 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt22L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1gt22));
    
    CCTK_REAL_VEC Dg222 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt22L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt22));
    
    CCTK_REAL_VEC Dg223 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt22L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt22));
    
    CCTK_REAL_VEC Dg231 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt23L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1gt23));
    
    CCTK_REAL_VEC Dg232 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt23L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt23));
    
    CCTK_REAL_VEC Dg233 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt23L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt23));
    
    CCTK_REAL_VEC Dg331 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt33L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1gt33));
    
    CCTK_REAL_VEC Dg332 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt33L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt33));
    
    CCTK_REAL_VEC Dg333 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(gt33L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt33));
    
    CCTK_REAL_VEC D2g1122 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth22gt11,kmadd(gt11L,kmul(ToReal(4),kmadd(kmul(Dphi2,Dphi2),ToReal(4),D2phi22)),kmul(Dphi2,kmul(JacPDstandardNth2gt11,ToReal(8))))));
    
    CCTK_REAL_VEC D2g1123 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt11L,D2phi23,kmadd(Dphi3,JacPDstandardNth2gt11,kmul(Dphi2,kmadd(gt11L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt11)))),JacPDstandardNth23gt11));
    
    CCTK_REAL_VEC D2g1133 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth33gt11,kmadd(gt11L,kmul(ToReal(4),kmadd(kmul(Dphi3,Dphi3),ToReal(4),D2phi33)),kmul(Dphi3,kmul(JacPDstandardNth3gt11,ToReal(8))))));
    
    CCTK_REAL_VEC D2g1212 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt12L,D2phi12,kmadd(Dphi2,JacPDstandardNth1gt12,kmul(Dphi1,kmadd(gt12L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt12)))),JacPDstandardNth12gt12));
    
    CCTK_REAL_VEC D2g1213 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt12L,D2phi13,kmadd(Dphi3,JacPDstandardNth1gt12,kmul(Dphi1,kmadd(gt12L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt12)))),JacPDstandardNth13gt12));
    
    CCTK_REAL_VEC D2g1223 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt12L,D2phi23,kmadd(Dphi3,JacPDstandardNth2gt12,kmul(Dphi2,kmadd(gt12L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt12)))),JacPDstandardNth23gt12));
    
    CCTK_REAL_VEC D2g1233 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth33gt12,kmadd(gt12L,kmul(ToReal(4),kmadd(kmul(Dphi3,Dphi3),ToReal(4),D2phi33)),kmul(Dphi3,kmul(JacPDstandardNth3gt12,ToReal(8))))));
    
    CCTK_REAL_VEC D2g1312 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt13L,D2phi12,kmadd(Dphi2,JacPDstandardNth1gt13,kmul(Dphi1,kmadd(gt13L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt13)))),JacPDstandardNth12gt13));
    
    CCTK_REAL_VEC D2g1313 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt13L,D2phi13,kmadd(Dphi3,JacPDstandardNth1gt13,kmul(Dphi1,kmadd(gt13L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt13)))),JacPDstandardNth13gt13));
    
    CCTK_REAL_VEC D2g1322 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth22gt13,kmadd(gt13L,kmul(ToReal(4),kmadd(kmul(Dphi2,Dphi2),ToReal(4),D2phi22)),kmul(Dphi2,kmul(JacPDstandardNth2gt13,ToReal(8))))));
    
    CCTK_REAL_VEC D2g1323 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt13L,D2phi23,kmadd(Dphi3,JacPDstandardNth2gt13,kmul(Dphi2,kmadd(gt13L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt13)))),JacPDstandardNth23gt13));
    
    CCTK_REAL_VEC D2g2211 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth11gt22,kmadd(gt22L,kmul(ToReal(4),kmadd(kmul(Dphi1,Dphi1),ToReal(4),D2phi11)),kmul(Dphi1,kmul(JacPDstandardNth1gt22,ToReal(8))))));
    
    CCTK_REAL_VEC D2g2213 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt22L,D2phi13,kmadd(Dphi3,JacPDstandardNth1gt22,kmul(Dphi1,kmadd(gt22L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt22)))),JacPDstandardNth13gt22));
    
    CCTK_REAL_VEC D2g2233 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth33gt22,kmadd(gt22L,kmul(ToReal(4),kmadd(kmul(Dphi3,Dphi3),ToReal(4),D2phi33)),kmul(Dphi3,kmul(JacPDstandardNth3gt22,ToReal(8))))));
    
    CCTK_REAL_VEC D2g2311 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth11gt23,kmadd(gt23L,kmul(ToReal(4),kmadd(kmul(Dphi1,Dphi1),ToReal(4),D2phi11)),kmul(Dphi1,kmul(JacPDstandardNth1gt23,ToReal(8))))));
    
    CCTK_REAL_VEC D2g2312 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt23L,D2phi12,kmadd(Dphi2,JacPDstandardNth1gt23,kmul(Dphi1,kmadd(gt23L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt23)))),JacPDstandardNth12gt23));
    
    CCTK_REAL_VEC D2g2313 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt23L,D2phi13,kmadd(Dphi3,JacPDstandardNth1gt23,kmul(Dphi1,kmadd(gt23L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt23)))),JacPDstandardNth13gt23));
    
    CCTK_REAL_VEC D2g2323 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt23L,D2phi23,kmadd(Dphi3,JacPDstandardNth2gt23,kmul(Dphi2,kmadd(gt23L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3gt23)))),JacPDstandardNth23gt23));
    
    CCTK_REAL_VEC D2g3311 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth11gt33,kmadd(gt33L,kmul(ToReal(4),kmadd(kmul(Dphi1,Dphi1),ToReal(4),D2phi11)),kmul(Dphi1,kmul(JacPDstandardNth1gt33,ToReal(8))))));
    
    CCTK_REAL_VEC D2g3312 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kmadd(ToReal(4),kmadd(gt33L,D2phi12,kmadd(Dphi2,JacPDstandardNth1gt33,kmul(Dphi1,kmadd(gt33L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2gt33)))),JacPDstandardNth12gt33));
    
    CCTK_REAL_VEC D2g3322 CCTK_ATTRIBUTE_UNUSED = 
      kmul(e4phi,kadd(JacPDstandardNth22gt33,kmadd(gt33L,kmul(ToReal(4),kmadd(kmul(Dphi2,Dphi2),ToReal(4),D2phi22)),kmul(Dphi2,kmul(JacPDstandardNth2gt33,ToReal(8))))));
    
    CCTK_REAL_VEC g11 CCTK_ATTRIBUTE_UNUSED = kmul(gt11L,e4phi);
    
    CCTK_REAL_VEC g12 CCTK_ATTRIBUTE_UNUSED = kmul(gt12L,e4phi);
    
    CCTK_REAL_VEC g13 CCTK_ATTRIBUTE_UNUSED = kmul(gt13L,e4phi);
    
    CCTK_REAL_VEC g22 CCTK_ATTRIBUTE_UNUSED = kmul(gt22L,e4phi);
    
    CCTK_REAL_VEC g23 CCTK_ATTRIBUTE_UNUSED = kmul(gt23L,e4phi);
    
    CCTK_REAL_VEC g33 CCTK_ATTRIBUTE_UNUSED = kmul(gt33L,e4phi);
    
    CCTK_REAL_VEC gu11 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(g23,g23,kmul(g22,g33)),kmadd(g22,kmul(g13,g13),kmadd(g11,kmul(g23,g23),kmadd(g12,kmul(g13,kmul(g23,ToReal(-2))),kmul(g33,kmsub(g12,g12,kmul(g11,g22)))))));
    
    CCTK_REAL_VEC gu12 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(g12,g33,kmul(g13,g23)),kmadd(g33,kmul(g12,g12),kmadd(g22,kmul(g13,g13),kmadd(g12,kmul(g13,kmul(g23,ToReal(-2))),kmul(g11,kmsub(g23,g23,kmul(g22,g33)))))));
    
    CCTK_REAL_VEC gu13 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(g13,g22,kmul(g12,g23)),kmadd(g22,kmul(g13,g13),kmadd(g11,kmul(g23,g23),kmadd(g12,kmul(g13,kmul(g23,ToReal(-2))),kmul(g33,kmsub(g12,g12,kmul(g11,g22)))))));
    
    CCTK_REAL_VEC gu22 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(g13,g13,kmul(g11,g33)),kmadd(g22,kmul(g13,g13),kmadd(g11,kmul(g23,g23),kmadd(g12,kmul(g13,kmul(g23,ToReal(-2))),kmul(g33,kmsub(g12,g12,kmul(g11,g22)))))));
    
    CCTK_REAL_VEC gu23 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(g11,g23,kmul(g12,g13)),kmadd(g33,kmul(g12,g12),kmadd(g22,kmul(g13,g13),kmadd(g12,kmul(g13,kmul(g23,ToReal(-2))),kmul(g11,kmsub(g23,g23,kmul(g22,g33)))))));
    
    CCTK_REAL_VEC gu33 CCTK_ATTRIBUTE_UNUSED = 
      kdiv(kmsub(g12,g12,kmul(g11,g22)),kmadd(g22,kmul(g13,g13),kmadd(g11,kmul(g23,g23),kmadd(g12,kmul(g13,kmul(g23,ToReal(-2))),kmul(g33,kmsub(g12,g12,kmul(g11,g22)))))));
    
    CCTK_REAL_VEC Gl111 CCTK_ATTRIBUTE_UNUSED = kmul(Dg111,ToReal(0.5));
    
    CCTK_REAL_VEC Gl112 CCTK_ATTRIBUTE_UNUSED = kmul(Dg112,ToReal(0.5));
    
    CCTK_REAL_VEC Gl113 CCTK_ATTRIBUTE_UNUSED = kmul(Dg113,ToReal(0.5));
    
    CCTK_REAL_VEC Gl122 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Dg221,ToReal(-0.5),Dg122);
    
    CCTK_REAL_VEC Gl123 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(Dg123,ksub(Dg132,Dg231)),ToReal(0.5));
    
    CCTK_REAL_VEC Gl133 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Dg331,ToReal(-0.5),Dg133);
    
    CCTK_REAL_VEC Gl211 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Dg112,ToReal(-0.5),Dg121);
    
    CCTK_REAL_VEC Gl212 CCTK_ATTRIBUTE_UNUSED = kmul(Dg221,ToReal(0.5));
    
    CCTK_REAL_VEC Gl213 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(Dg123,ksub(Dg231,Dg132)),ToReal(0.5));
    
    CCTK_REAL_VEC Gl222 CCTK_ATTRIBUTE_UNUSED = kmul(Dg222,ToReal(0.5));
    
    CCTK_REAL_VEC Gl223 CCTK_ATTRIBUTE_UNUSED = kmul(Dg223,ToReal(0.5));
    
    CCTK_REAL_VEC Gl233 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Dg332,ToReal(-0.5),Dg233);
    
    CCTK_REAL_VEC Gl311 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Dg113,ToReal(-0.5),Dg131);
    
    CCTK_REAL_VEC Gl312 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kadd(Dg132,ksub(Dg231,Dg123)),ToReal(0.5));
    
    CCTK_REAL_VEC Gl313 CCTK_ATTRIBUTE_UNUSED = kmul(Dg331,ToReal(0.5));
    
    CCTK_REAL_VEC Gl322 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Dg223,ToReal(-0.5),Dg232);
    
    CCTK_REAL_VEC Gl323 CCTK_ATTRIBUTE_UNUSED = kmul(Dg332,ToReal(0.5));
    
    CCTK_REAL_VEC Gl333 CCTK_ATTRIBUTE_UNUSED = kmul(Dg333,ToReal(0.5));
    
    CCTK_REAL_VEC G111 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl111,gu11,kmadd(Gl211,gu12,kmul(Gl311,gu13)));
    
    CCTK_REAL_VEC G211 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl111,gu12,kmadd(Gl211,gu22,kmul(Gl311,gu23)));
    
    CCTK_REAL_VEC G311 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl111,gu13,kmadd(Gl211,gu23,kmul(Gl311,gu33)));
    
    CCTK_REAL_VEC G112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl112,gu11,kmadd(Gl212,gu12,kmul(Gl312,gu13)));
    
    CCTK_REAL_VEC G212 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl112,gu12,kmadd(Gl212,gu22,kmul(Gl312,gu23)));
    
    CCTK_REAL_VEC G312 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl112,gu13,kmadd(Gl212,gu23,kmul(Gl312,gu33)));
    
    CCTK_REAL_VEC G113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl113,gu11,kmadd(Gl213,gu12,kmul(Gl313,gu13)));
    
    CCTK_REAL_VEC G213 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl113,gu12,kmadd(Gl213,gu22,kmul(Gl313,gu23)));
    
    CCTK_REAL_VEC G313 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl113,gu13,kmadd(Gl213,gu23,kmul(Gl313,gu33)));
    
    CCTK_REAL_VEC G122 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl122,gu11,kmadd(Gl222,gu12,kmul(Gl322,gu13)));
    
    CCTK_REAL_VEC G222 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl122,gu12,kmadd(Gl222,gu22,kmul(Gl322,gu23)));
    
    CCTK_REAL_VEC G322 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl122,gu13,kmadd(Gl222,gu23,kmul(Gl322,gu33)));
    
    CCTK_REAL_VEC G123 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl123,gu11,kmadd(Gl223,gu12,kmul(Gl323,gu13)));
    
    CCTK_REAL_VEC G223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl123,gu12,kmadd(Gl223,gu22,kmul(Gl323,gu23)));
    
    CCTK_REAL_VEC G323 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl123,gu13,kmadd(Gl223,gu23,kmul(Gl323,gu33)));
    
    CCTK_REAL_VEC G133 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl133,gu11,kmadd(Gl233,gu12,kmul(Gl333,gu13)));
    
    CCTK_REAL_VEC G233 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl133,gu12,kmadd(Gl233,gu22,kmul(Gl333,gu23)));
    
    CCTK_REAL_VEC G333 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Gl133,gu13,kmadd(Gl233,gu23,kmul(Gl333,gu33)));
    
    CCTK_REAL_VEC Riem1111 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G111,Gl112,kmadd(G211,Gl212,kmsub(G311,Gl312,kmadd(G112,Gl111,kmadd(G312,Gl311,kmul(G212,Gl211))))));
    
    CCTK_REAL_VEC Riem1113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G111,Gl113,kmadd(G211,Gl213,kmsub(G311,Gl313,kmadd(G113,Gl111,kmadd(G313,Gl311,kmul(G213,Gl211))))));
    
    CCTK_REAL_VEC Riem1121 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl111,kmadd(G212,Gl211,kmsub(G312,Gl311,kmadd(G111,Gl112,kmadd(G311,Gl312,kmul(G211,Gl212))))));
    
    CCTK_REAL_VEC Riem1122 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1123 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl113,kmadd(G212,Gl213,kmsub(G312,Gl313,kmadd(G113,Gl112,kmadd(G313,Gl312,kmul(G213,Gl212))))));
    
    CCTK_REAL_VEC Riem1131 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl111,kmadd(G213,Gl211,kmsub(G313,Gl311,kmadd(G111,Gl113,kmadd(G311,Gl313,kmul(G211,Gl213))))));
    
    CCTK_REAL_VEC Riem1132 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl112,kmadd(G213,Gl212,kmsub(G313,Gl312,kmadd(G112,Gl113,kmadd(G312,Gl313,kmul(G212,Gl213))))));
    
    CCTK_REAL_VEC Riem1133 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1211 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1212 CCTK_ATTRIBUTE_UNUSED = 
      kadd(D2g1212,kmadd(G112,Gl112,kmadd(G212,Gl212,kmadd(G312,Gl312,knmsub(G122,Gl111,knmsub(G222,Gl211,kmsub(kadd(D2g1122,D2g2211),ToReal(-0.5),kmul(G322,Gl311))))))));
    
    CCTK_REAL_VEC Riem1213 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl113,kmadd(G212,Gl213,kmadd(G312,Gl313,knmsub(G123,Gl111,knmsub(G223,Gl211,kmsub(kadd(D2g1213,ksub(D2g1312,kadd(D2g2311,D2g1123))),ToReal(0.5),kmul(G323,Gl311)))))));
    
    CCTK_REAL_VEC Riem1221 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G122,Gl111,kmadd(G222,Gl211,kmadd(G322,Gl311,knmsub(G112,Gl112,knmsub(G212,Gl212,kmsub(kadd(D2g1122,kmadd(D2g1212,ToReal(-2),D2g2211)),ToReal(0.5),kmul(G312,Gl312)))))));
    
    CCTK_REAL_VEC Riem1222 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G122,Gl113,kmadd(G222,Gl213,kmadd(G322,Gl313,knmsub(G123,Gl112,knmsub(G223,Gl212,kmsub(kadd(D2g1322,ksub(D2g2213,kadd(D2g2312,D2g1223))),ToReal(0.5),kmul(G323,Gl312)))))));
    
    CCTK_REAL_VEC Riem1231 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl111,kmadd(G223,Gl211,kmadd(G323,Gl311,knmsub(G112,Gl113,knmsub(G212,Gl213,kmsub(kadd(D2g1123,ksub(D2g2311,kadd(D2g1312,D2g1213))),ToReal(0.5),kmul(G312,Gl313)))))));
    
    CCTK_REAL_VEC Riem1232 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl112,kmadd(G223,Gl212,kmadd(G323,Gl312,knmsub(G122,Gl113,knmsub(G222,Gl213,kmsub(kadd(D2g1223,ksub(D2g2312,kadd(D2g2213,D2g1322))),ToReal(0.5),kmul(G322,Gl313)))))));
    
    CCTK_REAL_VEC Riem1233 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1311 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1312 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl112,kmadd(G213,Gl212,kmadd(G313,Gl312,knmsub(G123,Gl111,knmsub(G223,Gl211,kmsub(kadd(D2g1213,ksub(D2g1312,kadd(D2g2311,D2g1123))),ToReal(0.5),kmul(G323,Gl311)))))));
    
    CCTK_REAL_VEC Riem1313 CCTK_ATTRIBUTE_UNUSED = 
      kadd(D2g1313,kmadd(G113,Gl113,kmadd(G213,Gl213,kmadd(G313,Gl313,knmsub(G133,Gl111,knmsub(G233,Gl211,kmsub(kadd(D2g1133,D2g3311),ToReal(-0.5),kmul(G333,Gl311))))))));
    
    CCTK_REAL_VEC Riem1321 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl111,kmadd(G223,Gl211,kmadd(G323,Gl311,knmsub(G113,Gl112,knmsub(G213,Gl212,kmsub(kadd(D2g1123,ksub(D2g2311,kadd(D2g1312,D2g1213))),ToReal(0.5),kmul(G313,Gl312)))))));
    
    CCTK_REAL_VEC Riem1322 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem1323 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl113,kmadd(G223,Gl213,kmadd(G323,Gl313,knmsub(G133,Gl112,knmsub(G233,Gl212,kmsub(kadd(D2g1323,ksub(D2g2313,kadd(D2g3312,D2g1233))),ToReal(0.5),kmul(G333,Gl312)))))));
    
    CCTK_REAL_VEC Riem1331 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G133,Gl111,kmadd(G233,Gl211,kmadd(G333,Gl311,knmsub(G113,Gl113,knmsub(G213,Gl213,kmsub(kadd(D2g1133,kmadd(D2g1313,ToReal(-2),D2g3311)),ToReal(0.5),kmul(G313,Gl313)))))));
    
    CCTK_REAL_VEC Riem1332 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G133,Gl112,kmadd(G233,Gl212,kmadd(G333,Gl312,knmsub(G123,Gl113,knmsub(G223,Gl213,kmsub(kadd(D2g1233,ksub(D2g3312,kadd(D2g2313,D2g1323))),ToReal(0.5),kmul(G323,Gl313)))))));
    
    CCTK_REAL_VEC Riem1333 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2111 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G111,Gl122,kmadd(G211,Gl222,kmadd(G311,Gl322,knmsub(G112,Gl112,knmsub(G212,Gl212,kmsub(kadd(D2g1122,kmadd(D2g1212,ToReal(-2),D2g2211)),ToReal(0.5),kmul(G312,Gl312)))))));
    
    CCTK_REAL_VEC Riem2113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G111,Gl123,kmadd(G211,Gl223,kmadd(G311,Gl323,knmsub(G113,Gl112,knmsub(G213,Gl212,kmsub(kadd(D2g1123,ksub(D2g2311,kadd(D2g1312,D2g1213))),ToReal(0.5),kmul(G313,Gl312)))))));
    
    CCTK_REAL_VEC Riem2121 CCTK_ATTRIBUTE_UNUSED = 
      kadd(D2g1212,kmadd(G112,Gl112,kmadd(G212,Gl212,kmadd(G312,Gl312,knmsub(G111,Gl122,knmsub(G211,Gl222,kmsub(kadd(D2g1122,D2g2211),ToReal(-0.5),kmul(G311,Gl322))))))));
    
    CCTK_REAL_VEC Riem2122 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2123 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl123,kmadd(G212,Gl223,kmadd(G312,Gl323,knmsub(G113,Gl122,knmsub(G213,Gl222,kmsub(kadd(D2g1223,ksub(D2g2312,kadd(D2g2213,D2g1322))),ToReal(0.5),kmul(G313,Gl322)))))));
    
    CCTK_REAL_VEC Riem2131 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl112,kmadd(G213,Gl212,kmadd(G313,Gl312,knmsub(G111,Gl123,knmsub(G211,Gl223,kmsub(kadd(D2g1213,ksub(D2g1312,kadd(D2g2311,D2g1123))),ToReal(0.5),kmul(G311,Gl323)))))));
    
    CCTK_REAL_VEC Riem2132 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl122,kmadd(G213,Gl222,kmadd(G313,Gl322,knmsub(G112,Gl123,knmsub(G212,Gl223,kmsub(kadd(D2g1322,ksub(D2g2213,kadd(D2g2312,D2g1223))),ToReal(0.5),kmul(G312,Gl323)))))));
    
    CCTK_REAL_VEC Riem2133 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2211 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2212 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl122,kmadd(G212,Gl222,kmsub(G312,Gl322,kmadd(G122,Gl112,kmadd(G322,Gl312,kmul(G222,Gl212))))));
    
    CCTK_REAL_VEC Riem2213 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl123,kmadd(G212,Gl223,kmsub(G312,Gl323,kmadd(G123,Gl112,kmadd(G323,Gl312,kmul(G223,Gl212))))));
    
    CCTK_REAL_VEC Riem2221 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G122,Gl112,kmadd(G222,Gl212,kmsub(G322,Gl312,kmadd(G112,Gl122,kmadd(G312,Gl322,kmul(G212,Gl222))))));
    
    CCTK_REAL_VEC Riem2222 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G122,Gl123,kmadd(G222,Gl223,kmsub(G322,Gl323,kmadd(G123,Gl122,kmadd(G323,Gl322,kmul(G223,Gl222))))));
    
    CCTK_REAL_VEC Riem2231 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl112,kmadd(G223,Gl212,kmsub(G323,Gl312,kmadd(G112,Gl123,kmadd(G312,Gl323,kmul(G212,Gl223))))));
    
    CCTK_REAL_VEC Riem2232 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl122,kmadd(G223,Gl222,kmsub(G323,Gl322,kmadd(G122,Gl123,kmadd(G322,Gl323,kmul(G222,Gl223))))));
    
    CCTK_REAL_VEC Riem2233 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2311 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2312 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl122,kmadd(G213,Gl222,kmadd(G313,Gl322,knmsub(G123,Gl112,knmsub(G223,Gl212,kmsub(kadd(D2g1322,ksub(D2g2213,kadd(D2g2312,D2g1223))),ToReal(0.5),kmul(G323,Gl312)))))));
    
    CCTK_REAL_VEC Riem2313 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl123,kmadd(G213,Gl223,kmadd(G313,Gl323,knmsub(G133,Gl112,knmsub(G233,Gl212,kmsub(kadd(D2g1323,ksub(D2g2313,kadd(D2g3312,D2g1233))),ToReal(0.5),kmul(G333,Gl312)))))));
    
    CCTK_REAL_VEC Riem2321 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl112,kmadd(G223,Gl212,kmadd(G323,Gl312,knmsub(G113,Gl122,knmsub(G213,Gl222,kmsub(kadd(D2g1223,ksub(D2g2312,kadd(D2g2213,D2g1322))),ToReal(0.5),kmul(G313,Gl322)))))));
    
    CCTK_REAL_VEC Riem2322 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem2323 CCTK_ATTRIBUTE_UNUSED = 
      kadd(D2g2323,kmadd(G123,Gl123,kmadd(G223,Gl223,kmadd(G323,Gl323,knmsub(G133,Gl122,knmsub(G233,Gl222,kmsub(kadd(D2g2233,D2g3322),ToReal(-0.5),kmul(G333,Gl322))))))));
    
    CCTK_REAL_VEC Riem2331 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G133,Gl112,kmadd(G233,Gl212,kmadd(G333,Gl312,knmsub(G113,Gl123,knmsub(G213,Gl223,kmsub(kadd(D2g1233,ksub(D2g3312,kadd(D2g2313,D2g1323))),ToReal(0.5),kmul(G313,Gl323)))))));
    
    CCTK_REAL_VEC Riem2332 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G133,Gl122,kmadd(G233,Gl222,kmadd(G333,Gl322,knmsub(G123,Gl123,knmsub(G223,Gl223,kmsub(kadd(D2g2233,kmadd(D2g2323,ToReal(-2),D2g3322)),ToReal(0.5),kmul(G323,Gl323)))))));
    
    CCTK_REAL_VEC Riem2333 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3111 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G111,Gl123,kmadd(G211,Gl223,kmadd(G311,Gl323,knmsub(G112,Gl113,knmsub(G212,Gl213,kmsub(kadd(D2g1123,ksub(D2g2311,kadd(D2g1312,D2g1213))),ToReal(0.5),kmul(G312,Gl313)))))));
    
    CCTK_REAL_VEC Riem3113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G111,Gl133,kmadd(G211,Gl233,kmadd(G311,Gl333,knmsub(G113,Gl113,knmsub(G213,Gl213,kmsub(kadd(D2g1133,kmadd(D2g1313,ToReal(-2),D2g3311)),ToReal(0.5),kmul(G313,Gl313)))))));
    
    CCTK_REAL_VEC Riem3121 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl113,kmadd(G212,Gl213,kmadd(G312,Gl313,knmsub(G111,Gl123,knmsub(G211,Gl223,kmsub(kadd(D2g1213,ksub(D2g1312,kadd(D2g2311,D2g1123))),ToReal(0.5),kmul(G311,Gl323)))))));
    
    CCTK_REAL_VEC Riem3122 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3123 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl133,kmadd(G212,Gl233,kmadd(G312,Gl333,knmsub(G113,Gl123,knmsub(G213,Gl223,kmsub(kadd(D2g1233,ksub(D2g3312,kadd(D2g2313,D2g1323))),ToReal(0.5),kmul(G313,Gl323)))))));
    
    CCTK_REAL_VEC Riem3131 CCTK_ATTRIBUTE_UNUSED = 
      kadd(D2g1313,kmadd(G113,Gl113,kmadd(G213,Gl213,kmadd(G313,Gl313,knmsub(G111,Gl133,knmsub(G211,Gl233,kmsub(kadd(D2g1133,D2g3311),ToReal(-0.5),kmul(G311,Gl333))))))));
    
    CCTK_REAL_VEC Riem3132 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl123,kmadd(G213,Gl223,kmadd(G313,Gl323,knmsub(G112,Gl133,knmsub(G212,Gl233,kmsub(kadd(D2g1323,ksub(D2g2313,kadd(D2g3312,D2g1233))),ToReal(0.5),kmul(G312,Gl333)))))));
    
    CCTK_REAL_VEC Riem3133 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3211 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3212 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl123,kmadd(G212,Gl223,kmadd(G312,Gl323,knmsub(G122,Gl113,knmsub(G222,Gl213,kmsub(kadd(D2g1223,ksub(D2g2312,kadd(D2g2213,D2g1322))),ToReal(0.5),kmul(G322,Gl313)))))));
    
    CCTK_REAL_VEC Riem3213 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G112,Gl133,kmadd(G212,Gl233,kmadd(G312,Gl333,knmsub(G123,Gl113,knmsub(G223,Gl213,kmsub(kadd(D2g1233,ksub(D2g3312,kadd(D2g2313,D2g1323))),ToReal(0.5),kmul(G323,Gl313)))))));
    
    CCTK_REAL_VEC Riem3221 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G122,Gl113,kmadd(G222,Gl213,kmadd(G322,Gl313,knmsub(G112,Gl123,knmsub(G212,Gl223,kmsub(kadd(D2g1322,ksub(D2g2213,kadd(D2g2312,D2g1223))),ToReal(0.5),kmul(G312,Gl323)))))));
    
    CCTK_REAL_VEC Riem3222 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G122,Gl133,kmadd(G222,Gl233,kmadd(G322,Gl333,knmsub(G123,Gl123,knmsub(G223,Gl223,kmsub(kadd(D2g2233,kmadd(D2g2323,ToReal(-2),D2g3322)),ToReal(0.5),kmul(G323,Gl323)))))));
    
    CCTK_REAL_VEC Riem3231 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl113,kmadd(G223,Gl213,kmadd(G323,Gl313,knmsub(G112,Gl133,knmsub(G212,Gl233,kmsub(kadd(D2g1323,ksub(D2g2313,kadd(D2g3312,D2g1233))),ToReal(0.5),kmul(G312,Gl333)))))));
    
    CCTK_REAL_VEC Riem3232 CCTK_ATTRIBUTE_UNUSED = 
      kadd(D2g2323,kmadd(G123,Gl123,kmadd(G223,Gl223,kmadd(G323,Gl323,knmsub(G122,Gl133,knmsub(G222,Gl233,kmsub(kadd(D2g2233,D2g3322),ToReal(-0.5),kmul(G322,Gl333))))))));
    
    CCTK_REAL_VEC Riem3233 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3311 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3312 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl123,kmadd(G213,Gl223,kmsub(G313,Gl323,kmadd(G123,Gl113,kmadd(G323,Gl313,kmul(G223,Gl213))))));
    
    CCTK_REAL_VEC Riem3313 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G113,Gl133,kmadd(G213,Gl233,kmsub(G313,Gl333,kmadd(G133,Gl113,kmadd(G333,Gl313,kmul(G233,Gl213))))));
    
    CCTK_REAL_VEC Riem3321 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl113,kmadd(G223,Gl213,kmsub(G323,Gl313,kmadd(G113,Gl123,kmadd(G313,Gl323,kmul(G213,Gl223))))));
    
    CCTK_REAL_VEC Riem3322 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riem3323 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G123,Gl133,kmadd(G223,Gl233,kmsub(G323,Gl333,kmadd(G133,Gl123,kmadd(G333,Gl323,kmul(G233,Gl223))))));
    
    CCTK_REAL_VEC Riem3331 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G133,Gl113,kmadd(G233,Gl213,kmsub(G333,Gl313,kmadd(G113,Gl133,kmadd(G313,Gl333,kmul(G213,Gl233))))));
    
    CCTK_REAL_VEC Riem3332 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(G133,Gl123,kmadd(G233,Gl223,kmsub(G333,Gl323,kmadd(G123,Gl133,kmadd(G323,Gl333,kmul(G223,Gl233))))));
    
    CCTK_REAL_VEC Riem3333 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1111 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1112 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1113 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1121 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1122 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1123 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1131 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1132 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1133 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1211 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1212 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(kmul(gu12,gu12),kmul(gu12,gu12)),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(kmul(gu13,gu13),kmul(kmul(gu22,gu22),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu11,gu11),kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))),kmadd(gu11,kmul(gu13,kmul(gu22,kmadd(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmadd(kmul(gu23,gu23),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmadd(gu11,kmadd(gu22,kmul(ToReal(-2),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221)))),kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))))),kmul(gu13,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))))))),kmul(gu12,kmadd(gu13,kmul(gu22,kmadd(gu13,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kmul(kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323))),ToReal(2))))),kmul(gu11,kmadd(gu23,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu13,kmadd(gu22,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu23,kmul(kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu1213 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))),kmul(gu23,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu23,kmadd(gu23,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmadd(gu23,kmul(gu33,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(gu11,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu13,kmadd(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))))))))),kmadd(gu11,kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmadd(gu13,kmadd(gu22,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmul(gu22,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))))))),kmul(gu12,kmadd(gu11,kmadd(gu22,kmul(gu33,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232)))),kmadd(gu13,kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(ToReal(-2),kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321)))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu23,kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))))))),kmul(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmsub(gu13,kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332))))))),kmul(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu1221 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(kmul(gu12,gu12),kmul(gu12,gu12)),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(kmul(gu13,gu13),kmul(kmul(gu22,gu22),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))),kmadd(kmul(gu11,gu11),kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))))),kmadd(gu11,kmul(gu13,kmul(gu22,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(kmul(gu23,gu23),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323))),kmadd(gu13,kmadd(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))),kmul(gu11,kmadd(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu22,kmul(kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),ToReal(2)))))))),kmul(gu12,kmadd(gu13,kmul(gu22,kmadd(gu13,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))),kmul(gu23,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),ToReal(2))))),kmul(gu11,kmadd(gu23,kmadd(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu22,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kmul(kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu1222 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1223 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmadd(gu11,kmadd(kmsub(gu22,gu33,kmul(gu23,gu23)),kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))),kmul(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmadd(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu33,kmadd(gu22,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))),kmadd(gu13,kmul(gu22,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))),kmul(gu12,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu23,kmadd(gu23,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332))))),kmul(gu22,kmadd(gu23,kmul(ToReal(-2),kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321)))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323)))))))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu1231 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))),kmul(gu23,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu23,kmadd(gu23,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmadd(gu23,kmul(gu33,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))),kmadd(gu11,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu13,kmadd(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))))))))),kmadd(gu11,kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmadd(gu13,kmadd(gu22,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmul(gu22,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))))),kmul(gu12,kmadd(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu23,kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))),kmul(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312)))))))),kmul(gu11,kmadd(gu22,kmul(gu33,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223)))),kmadd(gu23,kmadd(gu23,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kmul(kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),ToReal(2)))))))))))))))));
    
    CCTK_REAL_VEC Riemu1232 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmadd(gu11,kmadd(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmadd(gu13,kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmul(gu33,kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313)))))))),kmadd(gu13,kmul(gu22,kmsub(gu13,kmadd(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu23,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmul(gu12,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu23,kmadd(gu23,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))),kmul(gu22,kmadd(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kmul(kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu1233 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1311 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1312 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu23,kmadd(gu23,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmadd(gu11,kmul(gu13,kmadd(gu22,kmadd(gu13,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmadd(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223)))))))),kmul(gu23,kmadd(gu13,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmul(gu23,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313)))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmadd(gu23,kmul(gu33,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(gu11,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu13,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))))))),kmul(gu12,kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmadd(gu13,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231)))),kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu33,kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))))))),kmul(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmsub(gu13,kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))))),kmul(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu1313 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu11,gu11),kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmadd(gu13,kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))),kmul(kmul(gu23,gu23),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu13,kmul(gu33,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223)))))),kmul(kmul(gu33,gu33),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(gu11,kmul(gu13,kmadd(gu23,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu33,kmul(kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),ToReal(2))))))),kmul(gu12,kmadd(gu11,kmadd(gu13,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221)))),kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))))),kmul(gu33,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))))),kmul(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmadd(gu13,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu23,kmul(gu33,kmul(kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323))),ToReal(2))))))))))))));
    
    CCTK_REAL_VEC Riemu1321 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmadd(gu11,kmul(gu13,kmadd(gu22,kmadd(gu13,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmadd(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232)))))))),kmul(gu23,kmadd(gu13,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmadd(gu23,kmul(gu33,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))),kmadd(gu11,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu13,kmadd(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))))))))),kmul(gu12,kmadd(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu22,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu23,kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332)))))))))),kmul(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmadd(gu33,kmadd(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kmul(kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),ToReal(2)))))))))))))))));
    
    CCTK_REAL_VEC Riemu1322 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu1323 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu33,kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmadd(gu11,kmsub(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))))),kmadd(gu13,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu22,kmadd(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321)))))))))),kmul(gu12,kmadd(gu33,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmsub(gu22,gu33,kmul(gu23,gu23))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))),kmul(gu33,kmadd(gu23,kmul(ToReal(-2),kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))),kmul(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu1331 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu11,gu11),kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(gu13,kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323)))))),kmul(kmul(gu23,gu23),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu13,kmul(gu33,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232)))))),kmul(kmul(gu33,gu33),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(gu11,kmul(gu13,kmadd(gu23,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu13,kmadd(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu33,kmul(kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),ToReal(2))))))),kmul(gu12,kmadd(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmadd(gu13,kmadd(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))),kmul(gu23,kmul(gu33,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),ToReal(2)))))),kmul(gu11,kmadd(gu33,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kmul(kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),ToReal(2))))))))))))));
    
    CCTK_REAL_VEC Riemu1332 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu33,kmadd(gu23,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))))),kmadd(gu11,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))),kmul(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu23,kmadd(gu23,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))))),kmadd(gu13,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))),kmul(gu22,kmadd(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312)))))))))),kmul(gu12,kmadd(gu33,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))),kmul(gu33,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kmul(kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))),ToReal(2)))))))))))))));
    
    CCTK_REAL_VEC Riemu1333 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2111 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2112 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(kmul(gu12,gu12),kmul(gu12,gu12)),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(kmul(gu13,gu13),kmul(kmul(gu22,gu22),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))),kmadd(kmul(gu11,gu11),kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))))),kmadd(gu11,kmul(gu13,kmul(gu22,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(kmul(gu23,gu23),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323))),kmadd(gu13,kmadd(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))),kmul(gu11,kmadd(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu22,kmul(kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),ToReal(2)))))))),kmul(gu12,kmadd(gu13,kmul(gu22,kmadd(gu13,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))),kmul(gu23,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),ToReal(2))))),kmul(gu11,kmadd(gu23,kmadd(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu22,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kmul(kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu2113 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))),kmul(gu23,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu23,kmadd(gu23,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmadd(gu23,kmul(gu33,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))),kmadd(gu11,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu13,kmadd(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))))))))),kmadd(gu11,kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmadd(gu13,kmadd(gu22,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmul(gu22,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))))),kmul(gu12,kmadd(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu23,kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))),kmul(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312)))))))),kmul(gu11,kmadd(gu22,kmul(gu33,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223)))),kmadd(gu23,kmadd(gu23,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kmul(kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),ToReal(2)))))))))))))))));
    
    CCTK_REAL_VEC Riemu2121 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(kmul(gu12,gu12),kmul(gu12,gu12)),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(kmul(gu13,gu13),kmul(kmul(gu22,gu22),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu11,gu11),kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))),kmadd(gu11,kmul(gu13,kmul(gu22,kmadd(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmadd(kmul(gu23,gu23),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmadd(gu11,kmadd(gu22,kmul(ToReal(-2),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221)))),kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))))),kmul(gu13,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))))))),kmul(gu12,kmadd(gu13,kmul(gu22,kmadd(gu13,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kmul(kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323))),ToReal(2))))),kmul(gu11,kmadd(gu23,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu13,kmadd(gu22,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu23,kmul(kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu2122 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2123 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmadd(gu11,kmadd(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmadd(gu13,kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmul(gu33,kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313)))))))),kmadd(gu13,kmul(gu22,kmsub(gu13,kmadd(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu23,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmul(gu12,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu23,kmadd(gu23,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))),kmul(gu22,kmadd(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kmul(kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu2131 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))),kmul(gu23,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu23,kmadd(gu23,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmadd(gu23,kmul(gu33,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(gu11,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu13,kmadd(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))))))))),kmadd(gu11,kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmadd(gu13,kmadd(gu22,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmul(gu22,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))))))),kmul(gu12,kmadd(gu11,kmadd(gu22,kmul(gu33,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232)))),kmadd(gu13,kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(ToReal(-2),kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321)))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu23,kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))))))),kmul(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmsub(gu13,kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332))))))),kmul(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu2132 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmadd(gu11,kmadd(kmsub(gu22,gu33,kmul(gu23,gu23)),kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))),kmul(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmadd(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu33,kmadd(gu22,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))),kmadd(gu13,kmul(gu22,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))),kmul(gu12,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu23,kmadd(gu23,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332))))),kmul(gu22,kmadd(gu23,kmul(ToReal(-2),kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321)))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323)))))))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu2133 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2211 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2212 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2213 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2221 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2222 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2223 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2231 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2232 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2233 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2311 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2312 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmadd(gu13,kmul(gu22,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332)))))))),kmadd(gu11,kmadd(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(kmsub(gu22,gu33,kmul(gu23,gu23)),kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmadd(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu33,kmadd(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312)))))))),kmul(gu12,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu23,kmadd(gu23,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))),kmul(gu22,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232)))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323)))))))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu2313 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu33,kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))))),kmadd(gu11,kmsub(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu23,kmadd(gu23,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmadd(gu12,kmadd(gu33,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmsub(gu22,gu33,kmul(gu23,gu23))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))),kmul(gu33,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332)))),kmul(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))))))))),kmul(gu13,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu22,kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313)))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu2321 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmadd(gu13,kmul(gu22,kmsub(gu13,kmadd(gu22,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu23,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmadd(gu13,kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231)))))))),kmul(gu33,kmadd(gu23,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321)))))))),kmadd(gu11,kmadd(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmul(gu12,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332))))),kmul(gu22,kmadd(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kmul(kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu2322 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu2323 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmsub(gu23,gu23,kmul(gu22,gu33))),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmsub(gu13,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu12,kmadd(gu22,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221)))),kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu33,kmul(kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),ToReal(2)))))))),kmul(gu12,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))))))))));
    
    CCTK_REAL_VEC Riemu2331 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu33,kmadd(gu23,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))))))),kmadd(gu11,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))),kmul(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu23,kmadd(gu23,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))))),kmadd(gu13,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))),kmul(gu22,kmadd(gu23,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))))),kmul(gu12,kmadd(gu33,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231)))))))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))),kmul(gu33,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kmul(kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),ToReal(2)))))))))))))));
    
    CCTK_REAL_VEC Riemu2332 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),knmsub(kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmsub(gu23,gu23,kmul(gu22,gu33))),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(gu12,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))))),kmul(gu13,kmadd(kmsub(gu22,gu33,kmul(gu23,gu23)),kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu12,kmadd(gu22,kmadd(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kmul(kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),ToReal(2)))),kmul(gu23,kmadd(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu33,kmul(kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),ToReal(2))))))))))))));
    
    CCTK_REAL_VEC Riemu2333 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3111 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3112 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmadd(gu11,kmul(gu13,kmadd(gu22,kmadd(gu13,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmadd(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232)))))))),kmul(gu23,kmadd(gu13,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmadd(gu23,kmul(gu33,kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))),kmadd(gu11,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu13,kmadd(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))),kmul(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))))))))),kmul(gu12,kmadd(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu22,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu23,kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332)))))))))),kmul(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmadd(gu33,kmadd(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kmul(kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),ToReal(2)))))))))))))))));
    
    CCTK_REAL_VEC Riemu3113 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu11,gu11),kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu13,gu13),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmadd(gu13,kmul(gu23,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323)))))),kmul(kmul(gu23,gu23),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu13,kmul(gu33,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232)))))),kmul(kmul(gu33,gu33),kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323)))))),kmadd(gu11,kmul(gu13,kmadd(gu23,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu13,kmadd(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu33,kmul(kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),ToReal(2))))))),kmul(gu12,kmadd(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmadd(gu13,kmadd(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))),kmul(gu23,kmul(gu33,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),ToReal(2)))))),kmul(gu11,kmadd(gu33,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kmul(kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),ToReal(2))))))))))))));
    
    CCTK_REAL_VEC Riemu3121 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu13,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))))),kmadd(gu22,kmul(kmul(gu13,gu13),kmadd(gu13,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu11,gu11),kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu23,kmadd(gu23,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmadd(gu11,kmul(gu13,kmadd(gu22,kmadd(gu13,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmadd(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223)))))))),kmul(gu23,kmadd(gu13,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),kmul(gu23,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313)))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmadd(gu23,kmul(gu33,kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(gu11,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu13,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))))))),kmul(gu12,kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmadd(gu13,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231)))),kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu33,kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))))))),kmul(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmsub(gu13,kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))))),kmul(kmadd(gu22,gu33,kmul(gu23,gu23)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu3122 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3123 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu33,kmadd(gu23,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313))))))),kmadd(gu11,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))),kmul(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu23,kmadd(gu23,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))))),kmadd(gu13,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))),kmul(gu22,kmadd(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu23,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312)))))))))),kmul(gu12,kmadd(gu33,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))),kmul(gu33,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kmul(kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))),ToReal(2)))))))))))))));
    
    CCTK_REAL_VEC Riemu3131 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu11,gu11),kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu13,gu13),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmadd(gu13,kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))),kmul(kmul(gu23,gu23),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu13,gu13),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu13,kmul(gu33,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223)))))),kmul(kmul(gu33,gu33),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(gu11,kmul(gu13,kmadd(gu23,kmadd(gu23,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu13,kmadd(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu33,kmul(kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),ToReal(2))))))),kmul(gu12,kmadd(gu11,kmadd(gu13,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221)))),kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))))),kmul(gu33,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))))),kmul(gu13,kmadd(kmul(gu13,gu13),kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmadd(gu13,kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))))),kmul(gu23,kmul(gu33,kmul(kadd(Riem2332,ksub(Riem3223,kadd(Riem3232,Riem2323))),ToReal(2))))))))))))));
    
    CCTK_REAL_VEC Riemu3132 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu33,kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmadd(gu11,kmsub(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))))),kmadd(gu13,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu22,kmadd(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321)))))))))),kmul(gu12,kmadd(gu33,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmsub(gu22,gu33,kmul(gu23,gu23))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))),kmul(gu33,kmadd(gu23,kmul(ToReal(-2),kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))),kmul(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu3133 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3211 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3212 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmadd(gu13,kmul(gu22,kmsub(gu13,kmadd(gu22,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu23,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))),kmadd(gu13,kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231)))))))),kmul(gu33,kmadd(gu23,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))),kmul(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321)))))))),kmadd(gu11,kmadd(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmul(gu12,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2331,kadd(Riem3132,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3123),Riem2313),Riem1332))))),kmul(gu22,kmadd(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu23,kmul(kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),ToReal(2))))))))))))))));
    
    CCTK_REAL_VEC Riemu3213 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu33,kmadd(gu23,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu33,kadd(Riem1332,ksub(Riem3123,kadd(Riem3132,Riem1323))))))),kmadd(gu11,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))),kmul(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))))),kmul(gu23,kmadd(gu23,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))))),kmadd(gu13,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))),kmul(gu23,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323))))),kmul(gu22,kmadd(gu23,kadd(Riem1232,ksub(Riem2123,kadd(Riem2132,Riem1223))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331)))))))))),kmul(gu12,kmadd(gu33,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),kmul(gu23,kadd(Riem1213,kadd(Riem1321,kadd(Riem2131,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2113),Riem1312),Riem1231)))))))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2123),Riem1232))))),kmul(gu33,kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kmul(kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),ToReal(2)))))))))))))));
    
    CCTK_REAL_VEC Riemu3221 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,kmul(gu12,gu12)),kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmadd(gu13,kmul(gu22,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmul(gu13,kmadd(gu22,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332)))))))),kmadd(gu11,kmadd(gu13,kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(kmsub(gu22,gu33,kmul(gu23,gu23)),kmadd(gu22,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu23,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmadd(gu13,kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu33,kmadd(gu23,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))),kmul(gu22,kadd(Riem2321,ksub(Riem3212,kadd(Riem3221,Riem2312)))))))),kmul(gu12,knmsub(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmadd(gu11,kmadd(gu22,kmadd(gu23,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmul(gu33,kadd(Riem1312,ksub(Riem3121,kadd(Riem3112,Riem1321))))),kmul(gu23,kmadd(gu23,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu33,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1332,kadd(Riem2313,kadd(Riem3123,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3132),Riem2331),Riem1323))))),kmul(gu22,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232)))),kmul(gu33,kadd(Riem1332,kadd(Riem2331,kadd(Riem3123,ksub(Riem3213,kadd(kadd(kadd(Riem3231,Riem3132),Riem2313),Riem1323)))))))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu3222 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3223 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),knmsub(kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmsub(gu23,gu23,kmul(gu22,gu33))),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu22,gu22),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu22,kmul(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu23,gu23),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmadd(gu23,kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213)))))),kmul(kmul(gu33,gu33),kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmadd(gu12,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332)))))))),kmul(gu13,kmadd(kmsub(gu22,gu33,kmul(gu23,gu23)),kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu12,kmadd(gu22,kmadd(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu23,kmul(kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),ToReal(2)))),kmul(gu23,kmadd(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231))))),kmul(gu33,kmul(kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))),ToReal(2))))))))))))));
    
    CCTK_REAL_VEC Riemu3231 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kmul(gu12,gu12),kmadd(gu13,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))))),kmul(gu33,kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332))))))),kmadd(gu11,kmsub(gu13,kmadd(gu22,kmadd(gu23,kadd(Riem1221,ksub(Riem2112,kadd(Riem2121,Riem1212))),kmul(gu33,kadd(Riem1231,ksub(Riem2113,kadd(Riem2131,Riem1213))))),kmul(gu23,kmadd(gu23,kadd(Riem1321,ksub(Riem3112,kadd(Riem3121,Riem1312))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))))))),kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem2312,ksub(Riem3221,kadd(Riem3212,Riem2321))),kmul(gu33,kadd(Riem2313,ksub(Riem3231,kadd(Riem3213,Riem2331))))))),kmadd(gu12,kmadd(gu33,kmul(kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmsub(gu22,gu33,kmul(gu23,gu23))),kmadd(gu11,kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kadd(Riem1231,kadd(Riem1312,kadd(Riem2113,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2131),Riem1321),Riem1213))))),kmul(gu33,kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313)))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1232,kadd(Riem2123,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2132),Riem1223))))),kmul(gu33,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1323,ksub(Riem3132,kadd(Riem3123,Riem1332)))),kmul(gu22,kadd(Riem1232,kadd(Riem2123,kadd(Riem2321,ksub(Riem3212,kadd(kadd(kadd(Riem3221,Riem2312),Riem2132),Riem1223))))))))))))),kmul(gu13,kmadd(gu23,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332)))),kmadd(kmul(gu13,gu13),kmadd(gu22,kadd(Riem1213,ksub(Riem2131,kadd(Riem2113,Riem1231))),kmul(gu23,kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331))))),kmul(gu13,kmadd(kmul(gu23,gu23),kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))),kmul(gu22,kmadd(gu23,kadd(Riem1223,ksub(Riem2132,kadd(Riem2123,Riem1232))),kmul(gu33,kadd(Riem2331,ksub(Riem3213,kadd(Riem3231,Riem2313)))))))))))))),ToReal(0.25));
    
    CCTK_REAL_VEC Riemu3232 CCTK_ATTRIBUTE_UNUSED = 
      kmul(ToReal(0.25),kmadd(kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmsub(gu23,gu23,kmul(gu22,gu33))),kadd(Riem2323,ksub(Riem3232,kadd(Riem3223,Riem2332))),kmadd(kmul(gu13,gu13),kmadd(kmul(gu22,gu22),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu22,kmul(gu23,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu23,gu23),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmadd(kmul(gu12,gu12),kmadd(kmul(gu23,gu23),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221))),kmadd(gu23,kmul(gu33,kadd(Riem1213,kadd(Riem1312,kadd(Riem2131,ksub(Riem3121,kadd(kadd(kadd(Riem3112,Riem2113),Riem1321),Riem1231)))))),kmul(kmul(gu33,gu33),kadd(Riem1313,ksub(Riem3131,kadd(Riem3113,Riem1331)))))),kmsub(gu13,kmadd(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu22,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu23,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))),kmul(gu12,kmadd(gu22,kmadd(gu23,kmul(ToReal(-2),kadd(Riem1212,ksub(Riem2121,kadd(Riem2112,Riem1221)))),kmul(gu33,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))))),kmul(gu23,kmadd(gu23,kadd(Riem1231,kadd(Riem1321,kadd(Riem2113,ksub(Riem3112,kadd(kadd(kadd(Riem3121,Riem2131),Riem1312),Riem1213))))),kmul(gu33,kmul(kadd(Riem1331,ksub(Riem3113,kadd(Riem3131,Riem1313))),ToReal(2)))))))),kmul(gu12,kmul(kmsub(gu23,gu23,kmul(gu22,gu33)),kmadd(gu23,kadd(Riem1223,kadd(Riem2132,kadd(Riem2312,ksub(Riem3221,kadd(kadd(kadd(Riem3212,Riem2321),Riem2123),Riem1232))))),kmul(gu33,kadd(Riem1323,kadd(Riem2313,kadd(Riem3132,ksub(Riem3231,kadd(kadd(kadd(Riem3213,Riem3123),Riem2331),Riem1332))))))))))))));
    
    CCTK_REAL_VEC Riemu3233 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3311 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3312 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3313 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3321 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3322 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3323 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3331 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3332 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC Riemu3333 CCTK_ATTRIBUTE_UNUSED = ToReal(0);
    
    CCTK_REAL_VEC R11 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,Riem1111,kmadd(gu12,kadd(Riem1121,Riem2111),kmadd(gu22,Riem2121,kmadd(gu13,kadd(Riem1131,Riem3111),kmadd(gu23,kadd(Riem2131,Riem3121),kmul(gu33,Riem3131))))));
    
    CCTK_REAL_VEC R12 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,Riem1112,kmadd(gu12,kadd(Riem1122,Riem2112),kmadd(gu22,Riem2122,kmadd(gu13,kadd(Riem1132,Riem3112),kmadd(gu23,kadd(Riem2132,Riem3122),kmul(gu33,Riem3132))))));
    
    CCTK_REAL_VEC R13 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,Riem1113,kmadd(gu12,kadd(Riem1123,Riem2113),kmadd(gu22,Riem2123,kmadd(gu13,kadd(Riem1133,Riem3113),kmadd(gu23,kadd(Riem2133,Riem3123),kmul(gu33,Riem3133))))));
    
    CCTK_REAL_VEC R22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,Riem1212,kmadd(gu12,kadd(Riem1222,Riem2212),kmadd(gu22,Riem2222,kmadd(gu13,kadd(Riem1232,Riem3212),kmadd(gu23,kadd(Riem2232,Riem3222),kmul(gu33,Riem3232))))));
    
    CCTK_REAL_VEC R23 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,Riem1213,kmadd(gu12,kadd(Riem1223,Riem2213),kmadd(gu22,Riem2223,kmadd(gu13,kadd(Riem1233,Riem3213),kmadd(gu23,kadd(Riem2233,Riem3223),kmul(gu33,Riem3233))))));
    
    CCTK_REAL_VEC R33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,Riem1313,kmadd(gu12,kadd(Riem1323,Riem2313),kmadd(gu22,Riem2323,kmadd(gu13,kadd(Riem1333,Riem3313),kmadd(gu23,kadd(Riem2333,Riem3323),kmul(gu33,Riem3333))))));
    
    CCTK_REAL_VEC K11 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At11L,e4phi,kmul(trKL,kmul(g11,ToReal(0.333333333333333333333333333333))));
    
    CCTK_REAL_VEC K12 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At12L,e4phi,kmul(trKL,kmul(g12,ToReal(0.333333333333333333333333333333))));
    
    CCTK_REAL_VEC K13 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At13L,e4phi,kmul(trKL,kmul(g13,ToReal(0.333333333333333333333333333333))));
    
    CCTK_REAL_VEC K22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At22L,e4phi,kmul(trKL,kmul(g22,ToReal(0.333333333333333333333333333333))));
    
    CCTK_REAL_VEC K23 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At23L,e4phi,kmul(trKL,kmul(g23,ToReal(0.333333333333333333333333333333))));
    
    CCTK_REAL_VEC K33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(At33L,e4phi,kmul(trKL,kmul(g33,ToReal(0.333333333333333333333333333333))));
    
    CCTK_REAL_VEC Km11 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,K11,kmadd(gu12,K12,kmul(gu13,K13)));
    
    CCTK_REAL_VEC Km21 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,K11,kmadd(gu22,K12,kmul(gu23,K13)));
    
    CCTK_REAL_VEC Km31 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,K11,kmadd(gu23,K12,kmul(gu33,K13)));
    
    CCTK_REAL_VEC Km12 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,K12,kmadd(gu12,K22,kmul(gu13,K23)));
    
    CCTK_REAL_VEC Km22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,K12,kmadd(gu22,K22,kmul(gu23,K23)));
    
    CCTK_REAL_VEC Km32 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,K12,kmadd(gu23,K22,kmul(gu33,K23)));
    
    CCTK_REAL_VEC Km13 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,K13,kmadd(gu12,K23,kmul(gu13,K33)));
    
    CCTK_REAL_VEC Km23 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,K13,kmadd(gu22,K23,kmul(gu23,K33)));
    
    CCTK_REAL_VEC Km33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,K13,kmadd(gu23,K23,kmul(gu33,K33)));
    
    CCTK_REAL_VEC Ku11 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,Km11,kmadd(gu12,Km12,kmul(gu13,Km13)));
    
    CCTK_REAL_VEC Ku12 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,Km11,kmadd(gu22,Km12,kmul(gu23,Km13)));
    
    CCTK_REAL_VEC Ku13 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,Km11,kmadd(gu23,Km12,kmul(gu33,Km13)));
    
    CCTK_REAL_VEC Ku22 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,Km21,kmadd(gu22,Km22,kmul(gu23,Km23)));
    
    CCTK_REAL_VEC Ku23 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,Km21,kmadd(gu23,Km22,kmul(gu33,Km23)));
    
    CCTK_REAL_VEC Ku33 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,Km31,kmadd(gu23,Km32,kmul(gu33,Km33)));
    
    CCTK_REAL_VEC CDK111 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G111,K11,kmadd(G211,K12,kmul(G311,K13))),ToReal(-2),kmadd(kmadd(trKL,Dg111,kmul(g11,JacPDstandardNth1trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At11L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1At11))));
    
    CCTK_REAL_VEC CDK112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G112,K11,kmadd(G212,K12,kmul(G312,K13))),ToReal(-2),kmadd(kmadd(trKL,Dg112,kmul(g11,JacPDstandardNth2trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At11L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2At11))));
    
    CCTK_REAL_VEC CDK113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G113,K11,kmadd(G213,K12,kmul(G313,K13))),ToReal(-2),kmadd(kmadd(trKL,Dg113,kmul(g11,JacPDstandardNth3trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At11L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3At11))));
    
    CCTK_REAL_VEC CDK121 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G112,K11,knmsub(G312,K13,knmsub(G211,K22,knmsub(G311,K23,knmsub(K12,kadd(G212,G111),kmadd(kmadd(trKL,Dg121,kmul(g12,JacPDstandardNth1trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At12L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1At12))))))));
    
    CCTK_REAL_VEC CDK122 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G122,K11,knmsub(G322,K13,knmsub(G212,K22,knmsub(G312,K23,knmsub(K12,kadd(G222,G112),kmadd(kmadd(trKL,Dg122,kmul(g12,JacPDstandardNth2trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At12L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2At12))))))));
    
    CCTK_REAL_VEC CDK123 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G123,K11,knmsub(G323,K13,knmsub(G213,K22,knmsub(G313,K23,knmsub(K12,kadd(G223,G113),kmadd(kmadd(trKL,Dg123,kmul(g12,JacPDstandardNth3trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At12L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3At12))))))));
    
    CCTK_REAL_VEC CDK131 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G113,K11,knmsub(G213,K12,knmsub(G211,K23,knmsub(G311,K33,knmsub(K13,kadd(G313,G111),kmadd(kmadd(trKL,Dg131,kmul(g13,JacPDstandardNth1trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At13L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1At13))))))));
    
    CCTK_REAL_VEC CDK132 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G123,K11,knmsub(G223,K12,knmsub(G212,K23,knmsub(G312,K33,knmsub(K13,kadd(G323,G112),kmadd(kmadd(trKL,Dg132,kmul(g13,JacPDstandardNth2trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At13L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2At13))))))));
    
    CCTK_REAL_VEC CDK133 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G133,K11,knmsub(G233,K12,knmsub(G213,K23,knmsub(G313,K33,knmsub(K13,kadd(G333,G113),kmadd(kmadd(trKL,Dg133,kmul(g13,JacPDstandardNth3trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At13L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3At13))))))));
    
    CCTK_REAL_VEC CDK221 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G112,K12,kmadd(G212,K22,kmul(G312,K23))),ToReal(-2),kmadd(kmadd(trKL,Dg221,kmul(g22,JacPDstandardNth1trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At22L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1At22))));
    
    CCTK_REAL_VEC CDK222 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G122,K12,kmadd(G222,K22,kmul(G322,K23))),ToReal(-2),kmadd(kmadd(trKL,Dg222,kmul(g22,JacPDstandardNth2trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At22L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2At22))));
    
    CCTK_REAL_VEC CDK223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G123,K12,kmadd(G223,K22,kmul(G323,K23))),ToReal(-2),kmadd(kmadd(trKL,Dg223,kmul(g22,JacPDstandardNth3trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At22L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3At22))));
    
    CCTK_REAL_VEC CDK231 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G113,K12,knmsub(G112,K13,knmsub(G213,K22,knmsub(G312,K33,knmsub(K23,kadd(G313,G212),kmadd(kmadd(trKL,Dg231,kmul(g23,JacPDstandardNth1trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At23L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1At23))))))));
    
    CCTK_REAL_VEC CDK232 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G123,K12,knmsub(G122,K13,knmsub(G223,K22,knmsub(G322,K33,knmsub(K23,kadd(G323,G222),kmadd(kmadd(trKL,Dg232,kmul(g23,JacPDstandardNth2trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At23L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2At23))))))));
    
    CCTK_REAL_VEC CDK233 CCTK_ATTRIBUTE_UNUSED = 
      knmsub(G133,K12,knmsub(G123,K13,knmsub(G233,K22,knmsub(G323,K33,knmsub(K23,kadd(G333,G223),kmadd(kmadd(trKL,Dg233,kmul(g23,JacPDstandardNth3trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At23L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3At23))))))));
    
    CCTK_REAL_VEC CDK331 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G113,K13,kmadd(G213,K23,kmul(G313,K33))),ToReal(-2),kmadd(kmadd(trKL,Dg331,kmul(g33,JacPDstandardNth1trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At33L,kmul(Dphi1,ToReal(4)),JacPDstandardNth1At33))));
    
    CCTK_REAL_VEC CDK332 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G123,K13,kmadd(G223,K23,kmul(G323,K33))),ToReal(-2),kmadd(kmadd(trKL,Dg332,kmul(g33,JacPDstandardNth2trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At33L,kmul(Dphi2,ToReal(4)),JacPDstandardNth2At33))));
    
    CCTK_REAL_VEC CDK333 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(G133,K13,kmadd(G233,K23,kmul(G333,K33))),ToReal(-2),kmadd(kmadd(trKL,Dg333,kmul(g33,JacPDstandardNth3trK)),ToReal(0.333333333333333333333333333333),kmul(e4phi,kmadd(At33L,kmul(Dphi3,ToReal(4)),JacPDstandardNth3At33))));
    
    CCTK_REAL_VEC CDKu112 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(CDK111,gu12,kmadd(CDK112,gu22,kmul(CDK113,gu23))),kmul(gu11,gu11),kmadd(CDK221,kmul(gu12,kmul(gu12,gu12)),kmadd(kmadd(CDK331,gu12,kmadd(CDK332,gu22,kmul(CDK333,gu23))),kmul(gu13,gu13),kmadd(kmadd(gu13,kmadd(kmadd(CDK132,gu11,kmul(CDK232,gu12)),gu22,kmul(kmadd(CDK133,gu11,kmul(CDK233,gu12)),gu23)),kmul(gu11,kmadd(gu12,kmadd(CDK131,gu13,kmadd(CDK122,gu22,kmul(CDK123,gu23))),kmul(CDK121,kmul(gu12,gu12))))),ToReal(2),kmul(kmul(gu12,gu12),kmadd(CDK222,gu22,kmadd(CDK223,gu23,kmul(CDK231,kmul(gu13,ToReal(2))))))))));
    
    CCTK_REAL_VEC CDKu113 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(CDK111,gu13,kmadd(CDK112,gu23,kmul(CDK113,gu33))),kmul(gu11,gu11),kmadd(kmadd(CDK221,gu13,kmadd(CDK222,gu23,kmul(CDK223,gu33))),kmul(gu12,gu12),kmadd(CDK331,kmul(gu13,kmul(gu13,gu13)),kmadd(kmadd(gu13,kmadd(kmadd(CDK132,gu11,kmul(CDK232,gu12)),gu23,kmul(kmadd(CDK133,gu11,kmul(CDK233,gu12)),gu33)),kmul(gu11,kmadd(gu12,kmadd(CDK121,gu13,kmadd(CDK122,gu23,kmul(CDK123,gu33))),kmul(CDK131,kmul(gu13,gu13))))),ToReal(2),kmul(kmul(gu13,gu13),kmadd(CDK332,gu23,kmadd(CDK333,gu33,kmul(CDK231,kmul(gu12,ToReal(2))))))))));
    
    CCTK_REAL_VEC CDKu121 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,kmadd(gu12,kmadd(CDK111,gu11,kmadd(CDK121,gu12,kmul(CDK131,gu13))),kmadd(kmadd(CDK121,gu11,kmadd(CDK221,gu12,kmul(CDK231,gu13))),gu22,kmul(kmadd(CDK131,gu11,kmadd(CDK231,gu12,kmul(CDK331,gu13))),gu23))),kmadd(gu12,kmadd(gu12,kmadd(CDK112,gu11,kmadd(CDK122,gu12,kmul(CDK132,gu13))),kmadd(kmadd(CDK122,gu11,kmadd(CDK222,gu12,kmul(CDK232,gu13))),gu22,kmul(kmadd(CDK132,gu11,kmadd(CDK232,gu12,kmul(CDK332,gu13))),gu23))),kmul(gu13,kmadd(gu12,kmadd(CDK113,gu11,kmadd(CDK123,gu12,kmul(CDK133,gu13))),kmadd(kmadd(CDK123,gu11,kmadd(CDK223,gu12,kmul(CDK233,gu13))),gu22,kmul(kmadd(CDK133,gu11,kmadd(CDK233,gu12,kmul(CDK333,gu13))),gu23))))));
    
    CCTK_REAL_VEC CDKu122 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,kmadd(gu12,kmadd(CDK111,gu11,kmadd(CDK121,gu12,kmul(CDK131,gu13))),kmadd(kmadd(CDK121,gu11,kmadd(CDK221,gu12,kmul(CDK231,gu13))),gu22,kmul(kmadd(CDK131,gu11,kmadd(CDK231,gu12,kmul(CDK331,gu13))),gu23))),kmadd(gu22,kmadd(gu12,kmadd(CDK112,gu11,kmadd(CDK122,gu12,kmul(CDK132,gu13))),kmadd(kmadd(CDK122,gu11,kmadd(CDK222,gu12,kmul(CDK232,gu13))),gu22,kmul(kmadd(CDK132,gu11,kmadd(CDK232,gu12,kmul(CDK332,gu13))),gu23))),kmul(gu23,kmadd(gu12,kmadd(CDK113,gu11,kmadd(CDK123,gu12,kmul(CDK133,gu13))),kmadd(kmadd(CDK123,gu11,kmadd(CDK223,gu12,kmul(CDK233,gu13))),gu22,kmul(kmadd(CDK133,gu11,kmadd(CDK233,gu12,kmul(CDK333,gu13))),gu23))))));
    
    CCTK_REAL_VEC CDKu123 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,kmadd(gu12,kmadd(CDK111,gu11,kmadd(CDK121,gu12,kmul(CDK131,gu13))),kmadd(kmadd(CDK121,gu11,kmadd(CDK221,gu12,kmul(CDK231,gu13))),gu22,kmul(kmadd(CDK131,gu11,kmadd(CDK231,gu12,kmul(CDK331,gu13))),gu23))),kmadd(gu23,kmadd(gu12,kmadd(CDK112,gu11,kmadd(CDK122,gu12,kmul(CDK132,gu13))),kmadd(kmadd(CDK122,gu11,kmadd(CDK222,gu12,kmul(CDK232,gu13))),gu22,kmul(kmadd(CDK132,gu11,kmadd(CDK232,gu12,kmul(CDK332,gu13))),gu23))),kmul(kmadd(gu12,kmadd(CDK113,gu11,kmadd(CDK123,gu12,kmul(CDK133,gu13))),kmadd(kmadd(CDK123,gu11,kmadd(CDK223,gu12,kmul(CDK233,gu13))),gu22,kmul(kmadd(CDK133,gu11,kmadd(CDK233,gu12,kmul(CDK333,gu13))),gu23))),gu33)));
    
    CCTK_REAL_VEC CDKu131 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,kmadd(gu13,kmadd(CDK111,gu11,kmadd(CDK121,gu12,kmul(CDK131,gu13))),kmadd(kmadd(CDK121,gu11,kmadd(CDK221,gu12,kmul(CDK231,gu13))),gu23,kmul(kmadd(CDK131,gu11,kmadd(CDK231,gu12,kmul(CDK331,gu13))),gu33))),kmadd(gu12,kmadd(gu13,kmadd(CDK112,gu11,kmadd(CDK122,gu12,kmul(CDK132,gu13))),kmadd(kmadd(CDK122,gu11,kmadd(CDK222,gu12,kmul(CDK232,gu13))),gu23,kmul(kmadd(CDK132,gu11,kmadd(CDK232,gu12,kmul(CDK332,gu13))),gu33))),kmul(gu13,kmadd(gu13,kmadd(CDK113,gu11,kmadd(CDK123,gu12,kmul(CDK133,gu13))),kmadd(kmadd(CDK123,gu11,kmadd(CDK223,gu12,kmul(CDK233,gu13))),gu23,kmul(kmadd(CDK133,gu11,kmadd(CDK233,gu12,kmul(CDK333,gu13))),gu33))))));
    
    CCTK_REAL_VEC CDKu132 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,kmadd(gu13,kmadd(CDK111,gu11,kmadd(CDK121,gu12,kmul(CDK131,gu13))),kmadd(kmadd(CDK121,gu11,kmadd(CDK221,gu12,kmul(CDK231,gu13))),gu23,kmul(kmadd(CDK131,gu11,kmadd(CDK231,gu12,kmul(CDK331,gu13))),gu33))),kmadd(gu22,kmadd(gu13,kmadd(CDK112,gu11,kmadd(CDK122,gu12,kmul(CDK132,gu13))),kmadd(kmadd(CDK122,gu11,kmadd(CDK222,gu12,kmul(CDK232,gu13))),gu23,kmul(kmadd(CDK132,gu11,kmadd(CDK232,gu12,kmul(CDK332,gu13))),gu33))),kmul(gu23,kmadd(gu13,kmadd(CDK113,gu11,kmadd(CDK123,gu12,kmul(CDK133,gu13))),kmadd(kmadd(CDK123,gu11,kmadd(CDK223,gu12,kmul(CDK233,gu13))),gu23,kmul(kmadd(CDK133,gu11,kmadd(CDK233,gu12,kmul(CDK333,gu13))),gu33))))));
    
    CCTK_REAL_VEC CDKu133 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,kmadd(gu13,kmadd(CDK111,gu11,kmadd(CDK121,gu12,kmul(CDK131,gu13))),kmadd(kmadd(CDK121,gu11,kmadd(CDK221,gu12,kmul(CDK231,gu13))),gu23,kmul(kmadd(CDK131,gu11,kmadd(CDK231,gu12,kmul(CDK331,gu13))),gu33))),kmadd(gu23,kmadd(gu13,kmadd(CDK112,gu11,kmadd(CDK122,gu12,kmul(CDK132,gu13))),kmadd(kmadd(CDK122,gu11,kmadd(CDK222,gu12,kmul(CDK232,gu13))),gu23,kmul(kmadd(CDK132,gu11,kmadd(CDK232,gu12,kmul(CDK332,gu13))),gu33))),kmul(gu33,kmadd(gu13,kmadd(CDK113,gu11,kmadd(CDK123,gu12,kmul(CDK133,gu13))),kmadd(kmadd(CDK123,gu11,kmadd(CDK223,gu12,kmul(CDK233,gu13))),gu23,kmul(kmadd(CDK133,gu11,kmadd(CDK233,gu12,kmul(CDK333,gu13))),gu33))))));
    
    CCTK_REAL_VEC CDKu221 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(CDK112,kmul(gu12,kmul(gu12,gu12)),kmadd(kmadd(CDK221,gu11,kmadd(CDK222,gu12,kmul(CDK223,gu13))),kmul(gu22,gu22),kmadd(kmadd(CDK331,gu11,kmadd(CDK332,gu12,kmul(CDK333,gu13))),kmul(gu23,gu23),kmadd(kmadd(kmadd(CDK133,kmul(gu12,gu13),kmul(CDK231,kmul(gu11,gu22))),gu23,kmadd(gu12,kmadd(kmadd(CDK121,gu11,kmul(CDK123,gu13)),gu22,kmul(CDK131,kmul(gu11,gu23))),kmul(gu22,kmadd(kmadd(CDK232,gu12,kmul(CDK233,gu13)),gu23,kmul(CDK122,kmul(gu12,gu12)))))),ToReal(2),kmul(kmul(gu12,gu12),kmadd(CDK111,gu11,kmadd(CDK113,gu13,kmul(CDK132,kmul(gu23,ToReal(2))))))))));
    
    CCTK_REAL_VEC CDKu223 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(CDK111,gu13,kmadd(CDK112,gu23,kmul(CDK113,gu33))),kmul(gu12,gu12),kmadd(kmadd(CDK221,gu13,kmadd(CDK222,gu23,kmul(CDK223,gu33))),kmul(gu22,gu22),kmadd(CDK332,kmul(gu23,kmul(gu23,gu23)),kmadd(kmadd(gu22,kmul(gu23,kmadd(CDK122,gu12,kmadd(CDK231,gu13,kmul(CDK233,gu33)))),kmul(gu12,kmadd(gu13,kmadd(CDK121,gu22,kmul(CDK131,gu23)),kmadd(kmadd(CDK123,gu22,kmul(CDK133,gu23)),gu33,kmul(CDK132,kmul(gu23,gu23)))))),ToReal(2),kmul(kmul(gu23,gu23),kmadd(CDK331,gu13,kmadd(CDK333,gu33,kmul(CDK232,kmul(gu22,ToReal(2))))))))));
    
    CCTK_REAL_VEC CDKu231 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu11,kmadd(gu13,kmadd(CDK111,gu12,kmadd(CDK121,gu22,kmul(CDK131,gu23))),kmadd(gu23,kmadd(CDK121,gu12,kmadd(CDK221,gu22,kmul(CDK231,gu23))),kmul(kmadd(CDK131,gu12,kmadd(CDK231,gu22,kmul(CDK331,gu23))),gu33))),kmadd(gu12,kmadd(gu13,kmadd(CDK112,gu12,kmadd(CDK122,gu22,kmul(CDK132,gu23))),kmadd(gu23,kmadd(CDK122,gu12,kmadd(CDK222,gu22,kmul(CDK232,gu23))),kmul(kmadd(CDK132,gu12,kmadd(CDK232,gu22,kmul(CDK332,gu23))),gu33))),kmul(gu13,kmadd(gu13,kmadd(CDK113,gu12,kmadd(CDK123,gu22,kmul(CDK133,gu23))),kmadd(gu23,kmadd(CDK123,gu12,kmadd(CDK223,gu22,kmul(CDK233,gu23))),kmul(kmadd(CDK133,gu12,kmadd(CDK233,gu22,kmul(CDK333,gu23))),gu33))))));
    
    CCTK_REAL_VEC CDKu232 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu12,kmadd(gu13,kmadd(CDK111,gu12,kmadd(CDK121,gu22,kmul(CDK131,gu23))),kmadd(gu23,kmadd(CDK121,gu12,kmadd(CDK221,gu22,kmul(CDK231,gu23))),kmul(kmadd(CDK131,gu12,kmadd(CDK231,gu22,kmul(CDK331,gu23))),gu33))),kmadd(gu22,kmadd(gu13,kmadd(CDK112,gu12,kmadd(CDK122,gu22,kmul(CDK132,gu23))),kmadd(gu23,kmadd(CDK122,gu12,kmadd(CDK222,gu22,kmul(CDK232,gu23))),kmul(kmadd(CDK132,gu12,kmadd(CDK232,gu22,kmul(CDK332,gu23))),gu33))),kmul(gu23,kmadd(gu13,kmadd(CDK113,gu12,kmadd(CDK123,gu22,kmul(CDK133,gu23))),kmadd(gu23,kmadd(CDK123,gu12,kmadd(CDK223,gu22,kmul(CDK233,gu23))),kmul(kmadd(CDK133,gu12,kmadd(CDK233,gu22,kmul(CDK333,gu23))),gu33))))));
    
    CCTK_REAL_VEC CDKu233 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(gu13,kmadd(gu13,kmadd(CDK111,gu12,kmadd(CDK121,gu22,kmul(CDK131,gu23))),kmadd(gu23,kmadd(CDK121,gu12,kmadd(CDK221,gu22,kmul(CDK231,gu23))),kmul(kmadd(CDK131,gu12,kmadd(CDK231,gu22,kmul(CDK331,gu23))),gu33))),kmadd(gu23,kmadd(gu13,kmadd(CDK112,gu12,kmadd(CDK122,gu22,kmul(CDK132,gu23))),kmadd(gu23,kmadd(CDK122,gu12,kmadd(CDK222,gu22,kmul(CDK232,gu23))),kmul(kmadd(CDK132,gu12,kmadd(CDK232,gu22,kmul(CDK332,gu23))),gu33))),kmul(gu33,kmadd(gu13,kmadd(CDK113,gu12,kmadd(CDK123,gu22,kmul(CDK133,gu23))),kmadd(gu23,kmadd(CDK123,gu12,kmadd(CDK223,gu22,kmul(CDK233,gu23))),kmul(kmadd(CDK133,gu12,kmadd(CDK233,gu22,kmul(CDK333,gu23))),gu33))))));
    
    CCTK_REAL_VEC CDKu331 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(CDK113,kmul(gu13,kmul(gu13,gu13)),kmadd(kmadd(CDK221,gu11,kmadd(CDK222,gu12,kmul(CDK223,gu13))),kmul(gu23,gu23),kmadd(kmadd(CDK331,gu11,kmadd(CDK332,gu12,kmul(CDK333,gu13))),kmul(gu33,gu33),kmadd(kmadd(kmadd(CDK132,kmul(gu12,gu13),kmul(CDK231,kmul(gu11,gu23))),gu33,kmadd(gu13,kmadd(kmadd(CDK121,gu11,kmul(CDK122,gu12)),gu23,kmul(CDK131,kmul(gu11,gu33))),kmul(gu23,kmadd(kmadd(CDK232,gu12,kmul(CDK233,gu13)),gu33,kmul(CDK123,kmul(gu13,gu13)))))),ToReal(2),kmul(kmul(gu13,gu13),kmadd(CDK111,gu11,kmadd(CDK112,gu12,kmul(CDK133,kmul(gu33,ToReal(2))))))))));
    
    CCTK_REAL_VEC CDKu332 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(kmadd(CDK111,gu12,kmadd(CDK112,gu22,kmul(CDK113,gu23))),kmul(gu13,gu13),kmadd(CDK223,kmul(gu23,kmul(gu23,gu23)),kmadd(kmadd(CDK331,gu12,kmadd(CDK332,gu22,kmul(CDK333,gu23))),kmul(gu33,gu33),kmadd(kmadd(kmadd(CDK132,kmul(gu13,gu22),kmul(kmadd(CDK231,gu12,kmadd(CDK133,gu13,kmul(CDK232,gu22))),gu23)),gu33,kmul(gu13,kmadd(kmadd(CDK121,gu12,kmul(CDK122,gu22)),gu23,kmadd(CDK131,kmul(gu12,gu33),kmul(CDK123,kmul(gu23,gu23)))))),ToReal(2),kmul(kmul(gu23,gu23),kmadd(CDK221,gu12,kmadd(CDK222,gu22,kmul(CDK233,kmul(gu33,ToReal(2))))))))));
    
    CCTK_REAL_VEC term1 CCTK_ATTRIBUTE_UNUSED = 
      kmadd(Riem1111,Riemu1111,kmadd(Riem1112,Riemu1112,kmadd(Riem1113,Riemu1113,kmadd(Riem1121,Riemu1121,kmadd(Riem1122,Riemu1122,kmadd(Riem1123,Riemu1123,kmadd(Riem1131,Riemu1131,kmadd(Riem1132,Riemu1132,kmadd(Riem1133,Riemu1133,kmadd(Riem1211,Riemu1211,kmadd(Riem1222,Riemu1222,kmadd(Riem1233,Riemu1233,kmadd(Riem1311,Riemu1311,kmadd(Riem1322,Riemu1322,kmadd(Riem1333,Riemu1333,kmadd(Riem2111,Riemu2111,kmadd(Riem2122,Riemu2122,kmadd(Riem2133,Riemu2133,kmadd(Riem2211,Riemu2211,kmadd(Riem2212,Riemu2212,kmadd(Riem2213,Riemu2213,kmadd(Riem2221,Riemu2221,kmadd(Riem2222,Riemu2222,kmadd(Riem2223,Riemu2223,kmadd(Riem2231,Riemu2231,kmadd(Riem2232,Riemu2232,kmadd(Riem2233,Riemu2233,kmadd(Riem2311,Riemu2311,kmadd(Riem2322,Riemu2322,kmadd(Riem2333,Riemu2333,kmadd(Riem3111,Riemu3111,kmadd(Riem3122,Riemu3122,kmadd(Riem3133,Riemu3133,kmadd(Riem3211,Riemu3211,kmadd(Riem3222,Riemu3222,kmadd(Riem3233,Riemu3233,kmadd(Riem3311,Riemu3311,kmadd(Riem3312,Riemu3312,kmadd(Riem3313,Riemu3313,kmadd(Riem3321,Riemu3321,kmadd(Riem3322,Riemu3322,kmadd(Riem3323,Riemu3323,kmadd(Riem3331,Riemu3331,kmadd(Riem3332,Riemu3332,kmadd(Riem3333,Riemu3333,kmadd(kmadd(K11,K23,knmsub(K12,K13,Riem1213)),kmadd(Ku11,Ku23,knmsub(Ku12,Ku13,Riemu1213)),kmadd(kmadd(K11,K23,knmsub(K12,K13,Riem1312)),kmadd(Ku11,Ku23,knmsub(Ku12,Ku13,Riemu1312)),kmadd(kmadd(K11,K23,knmsub(K12,K13,Riem2131)),kmadd(Ku11,Ku23,knmsub(Ku12,Ku13,Riemu2131)),kmadd(kmadd(K11,K23,knmsub(K12,K13,Riem3121)),kmadd(Ku11,Ku23,knmsub(Ku12,Ku13,Riemu3121)),kmadd(kadd(Riem1221,kmsub(K12,K12,kmul(K11,K22))),kadd(Riemu1221,kmsub(Ku12,Ku12,kmul(Ku11,Ku22))),kmadd(kadd(Riem2112,kmsub(K12,K12,kmul(K11,K22))),kadd(Riemu2112,kmsub(Ku12,Ku12,kmul(Ku11,Ku22))),kmadd(kmadd(K12,K23,knmsub(K13,K22,Riem1223)),kmadd(Ku12,Ku23,knmsub(Ku13,Ku22,Riemu1223)),kmadd(kmadd(K12,K23,knmsub(K13,K22,Riem2132)),kmadd(Ku12,Ku23,knmsub(Ku13,Ku22,Riemu2132)),kmadd(kmadd(K12,K23,knmsub(K13,K22,Riem2312)),kmadd(Ku12,Ku23,knmsub(Ku13,Ku22,Riemu2312)),kmadd(kmadd(K12,K23,knmsub(K13,K22,Riem3221)),kmadd(Ku12,Ku23,knmsub(Ku13,Ku22,Riemu3221)),kmadd(kmadd(K12,K13,knmsub(K11,K23,Riem1231)),kmadd(Ku12,Ku13,knmsub(Ku11,Ku23,Riemu1231)),kmadd(kmadd(K12,K13,knmsub(K11,K23,Riem1321)),kmadd(Ku12,Ku13,knmsub(Ku11,Ku23,Riemu1321)),kmadd(kmadd(K12,K13,knmsub(K11,K23,Riem2113)),kmadd(Ku12,Ku13,knmsub(Ku11,Ku23,Riemu2113)),kmadd(kmadd(K12,K13,knmsub(K11,K23,Riem3112)),kmadd(Ku12,Ku13,knmsub(Ku11,Ku23,Riemu3112)),kmadd(kmadd(K13,K22,knmsub(K12,K23,Riem1232)),kmadd(Ku13,Ku22,knmsub(Ku12,Ku23,Riemu1232)),kmadd(kmadd(K13,K22,knmsub(K12,K23,Riem2123)),kmadd(Ku13,Ku22,knmsub(Ku12,Ku23,Riemu2123)),kmadd(kmadd(K13,K22,knmsub(K12,K23,Riem2321)),kmadd(Ku13,Ku22,knmsub(Ku12,Ku23,Riemu2321)),kmadd(kmadd(K13,K22,knmsub(K12,K23,Riem3212)),kmadd(Ku13,Ku22,knmsub(Ku12,Ku23,Riemu3212)),kmadd(kmadd(K12,K33,knmsub(K13,K23,Riem1323)),kmadd(Ku12,Ku33,knmsub(Ku13,Ku23,Riemu1323)),kmadd(kmadd(K12,K33,knmsub(K13,K23,Riem2313)),kmadd(Ku12,Ku33,knmsub(Ku13,Ku23,Riemu2313)),kmadd(kmadd(K12,K33,knmsub(K13,K23,Riem3132)),kmadd(Ku12,Ku33,knmsub(Ku13,Ku23,Riemu3132)),kmadd(kmadd(K12,K33,knmsub(K13,K23,Riem3231)),kmadd(Ku12,Ku33,knmsub(Ku13,Ku23,Riemu3231)),kmadd(kadd(Riem1331,kmsub(K13,K13,kmul(K11,K33))),kadd(Riemu1331,kmsub(Ku13,Ku13,kmul(Ku11,Ku33))),kmadd(kadd(Riem3113,kmsub(K13,K13,kmul(K11,K33))),kadd(Riemu3113,kmsub(Ku13,Ku13,kmul(Ku11,Ku33))),kmadd(kmadd(K13,K23,knmsub(K12,K33,Riem1332)),kmadd(Ku13,Ku23,knmsub(Ku12,Ku33,Riemu1332)),kmadd(kmadd(K13,K23,knmsub(K12,K33,Riem2331)),kmadd(Ku13,Ku23,knmsub(Ku12,Ku33,Riemu2331)),kmadd(kmadd(K13,K23,knmsub(K12,K33,Riem3123)),kmadd(Ku13,Ku23,knmsub(Ku12,Ku33,Riemu3123)),kmadd(kmadd(K13,K23,knmsub(K12,K33,Riem3213)),kmadd(Ku13,Ku23,knmsub(Ku12,Ku33,Riemu3213)),kmadd(kadd(Riem2332,kmsub(K23,K23,kmul(K22,K33))),kadd(Riemu2332,kmsub(Ku23,Ku23,kmul(Ku22,Ku33))),kmadd(kadd(Riem3223,kmsub(K23,K23,kmul(K22,K33))),kadd(Riemu3223,kmsub(Ku23,Ku23,kmul(Ku22,Ku33))),kmadd(kmadd(K11,K22,knmsub(K12,K12,Riem1212)),kmadd(Ku11,Ku22,knmsub(Ku12,Ku12,Riemu1212)),kmadd(kmadd(K11,K22,knmsub(K12,K12,Riem2121)),kmadd(Ku11,Ku22,knmsub(Ku12,Ku12,Riemu2121)),kmadd(kmadd(K11,K33,knmsub(K13,K13,Riem1313)),kmadd(Ku11,Ku33,knmsub(Ku13,Ku13,Riemu1313)),kmadd(kmadd(K11,K33,knmsub(K13,K13,Riem3131)),kmadd(Ku11,Ku33,knmsub(Ku13,Ku13,Riemu3131)),kmadd(kmadd(K22,K33,knmsub(K23,K23,Riem2323)),kmadd(Ku22,Ku33,knmsub(Ku23,Ku23,Riemu2323)),kmul(kmadd(K22,K33,knmsub(K23,K23,Riem3232)),kmadd(Ku22,Ku33,knmsub(Ku23,Ku23,Riemu3232)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
    
    CCTK_REAL_VEC term2 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(CDKu123,kadd(CDK132,kmadd(CDK123,ToReal(-2),CDK231)),kmadd(CDKu132,kadd(CDK123,kmadd(CDK132,ToReal(-2),CDK231)),kmadd(CDKu231,kadd(CDK123,kmadd(CDK231,ToReal(-2),CDK132)),kmadd(CDKu113,ksub(CDK131,CDK113),kmadd(CDKu122,ksub(CDK221,CDK122),kmadd(CDKu131,ksub(CDK113,CDK131),kmadd(CDKu133,ksub(CDK331,CDK133),kmadd(CDKu221,ksub(CDK122,CDK221),kmadd(CDKu223,ksub(CDK232,CDK223),kmadd(CDKu232,ksub(CDK223,CDK232),kmadd(CDKu233,ksub(CDK332,CDK233),kmadd(CDKu331,ksub(CDK133,CDK331),kmadd(CDKu332,ksub(CDK233,CDK332),kmadd(CDK112,ksub(CDKu121,CDKu112),kmul(CDK121,ksub(CDKu112,CDKu121)))))))))))))))),ToReal(8));
    
    CCTK_REAL_VEC term3 CCTK_ATTRIBUTE_UNUSED = 
      kmul(kmadd(kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),knmsub(gu12,kmul(gu13,kmadd(K13,Km12,kmadd(K12,Km13,kmadd(K23,Km22,kmadd(K22,Km23,kmadd(K33,Km32,kmadd(K23,Km33,kmadd(trKL,kmul(K23,ToReal(-2)),kmul(R23,ToReal(-2)))))))))),knmsub(gu11,kmadd(gu12,kmadd(K11,Km12,kmadd(K22,Km21,kmadd(K23,Km31,kmadd(K13,Km32,kmadd(R12,ToReal(-2),kmul(K12,kadd(Km11,kmadd(trKL,ToReal(-2),Km22)))))))),kmul(gu13,kmadd(K11,Km13,kmadd(K23,Km21,kmadd(K12,Km23,kmadd(K33,Km31,kmadd(R13,ToReal(-2),kmul(K13,kadd(Km11,kmadd(trKL,ToReal(-2),Km33)))))))))),kmadd(kmul(gu11,gu11),kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(kmul(gu12,gu12),kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(kmul(gu13,gu13),kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23))))))))),kmadd(kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),knmsub(gu22,kmul(gu23,kmadd(K13,Km12,kmadd(K12,Km13,kmadd(K23,Km22,kmadd(K22,Km23,kmadd(K33,Km32,kmadd(K23,Km33,kmadd(trKL,kmul(K23,ToReal(-2)),kmul(R23,ToReal(-2)))))))))),knmsub(gu12,kmadd(gu22,kmadd(K11,Km12,kmadd(K22,Km21,kmadd(K23,Km31,kmadd(K13,Km32,kmadd(R12,ToReal(-2),kmul(K12,kadd(Km11,kmadd(trKL,ToReal(-2),Km22)))))))),kmul(gu23,kmadd(K11,Km13,kmadd(K23,Km21,kmadd(K12,Km23,kmadd(K33,Km31,kmadd(R13,ToReal(-2),kmul(K13,kadd(Km11,kmadd(trKL,ToReal(-2),Km33)))))))))),kmadd(kmul(gu12,gu12),kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(kmul(gu22,gu22),kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(kmul(gu23,gu23),kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23))))))))),kmadd(kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23)))),knmsub(gu23,kmul(gu33,kmadd(K13,Km12,kmadd(K12,Km13,kmadd(K23,Km22,kmadd(K22,Km23,kmadd(K33,Km32,kmadd(K23,Km33,kmadd(trKL,kmul(K23,ToReal(-2)),kmul(R23,ToReal(-2)))))))))),knmsub(gu13,kmadd(gu23,kmadd(K11,Km12,kmadd(K22,Km21,kmadd(K23,Km31,kmadd(K13,Km32,kmadd(R12,ToReal(-2),kmul(K12,kadd(Km11,kmadd(trKL,ToReal(-2),Km22)))))))),kmul(gu33,kmadd(K11,Km13,kmadd(K23,Km21,kmadd(K12,Km23,kmadd(K33,Km31,kmadd(R13,ToReal(-2),kmul(K13,kadd(Km11,kmadd(trKL,ToReal(-2),Km33)))))))))),kmadd(kmul(gu13,gu13),kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(kmul(gu23,gu23),kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(kmul(gu33,gu33),kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23))))))))),kmadd(kadd(R12,knmsub(K11,Km12,kmsub(K12,ksub(trKL,Km22),kmul(K13,Km32)))),kmadd(gu12,kmadd(gu11,kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(gu12,kadd(R12,knmsub(K22,Km21,kmsub(K12,ksub(trKL,Km11),kmul(K23,Km31)))),kmul(gu13,kadd(R13,knmsub(K23,Km21,kmsub(K13,ksub(trKL,Km11),kmul(K33,Km31))))))),kmadd(gu22,kmadd(gu11,kadd(R12,knmsub(K11,Km12,kmsub(K12,ksub(trKL,Km22),kmul(K13,Km32)))),kmadd(gu12,kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(gu13,kadd(R23,knmsub(K13,Km12,kmsub(K23,ksub(trKL,Km22),kmul(K33,Km32))))))),kmul(gu23,kmadd(gu11,kadd(R13,knmsub(K11,Km13,kmsub(K13,ksub(trKL,Km33),kmul(K12,Km23)))),kmadd(gu12,kadd(R23,knmsub(K12,Km13,kmsub(K23,ksub(trKL,Km33),kmul(K22,Km23)))),kmul(gu13,kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23)))))))))),kmadd(kadd(R13,knmsub(K11,Km13,kmsub(K13,ksub(trKL,Km33),kmul(K12,Km23)))),kmadd(gu13,kmadd(gu11,kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(gu12,kadd(R12,knmsub(K22,Km21,kmsub(K12,ksub(trKL,Km11),kmul(K23,Km31)))),kmul(gu13,kadd(R13,knmsub(K23,Km21,kmsub(K13,ksub(trKL,Km11),kmul(K33,Km31))))))),kmadd(gu23,kmadd(gu11,kadd(R12,knmsub(K11,Km12,kmsub(K12,ksub(trKL,Km22),kmul(K13,Km32)))),kmadd(gu12,kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(gu13,kadd(R23,knmsub(K13,Km12,kmsub(K23,ksub(trKL,Km22),kmul(K33,Km32))))))),kmul(gu33,kmadd(gu11,kadd(R13,knmsub(K11,Km13,kmsub(K13,ksub(trKL,Km33),kmul(K12,Km23)))),kmadd(gu12,kadd(R23,knmsub(K12,Km13,kmsub(K23,ksub(trKL,Km33),kmul(K22,Km23)))),kmul(gu13,kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23)))))))))),kmadd(kadd(R12,knmsub(K22,Km21,kmsub(K12,ksub(trKL,Km11),kmul(K23,Km31)))),kmadd(gu11,kmadd(gu12,kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(gu22,kadd(R12,knmsub(K22,Km21,kmsub(K12,ksub(trKL,Km11),kmul(K23,Km31)))),kmul(gu23,kadd(R13,knmsub(K23,Km21,kmsub(K13,ksub(trKL,Km11),kmul(K33,Km31))))))),kmadd(gu12,kmadd(gu12,kadd(R12,knmsub(K11,Km12,kmsub(K12,ksub(trKL,Km22),kmul(K13,Km32)))),kmadd(gu22,kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(gu23,kadd(R23,knmsub(K13,Km12,kmsub(K23,ksub(trKL,Km22),kmul(K33,Km32))))))),kmul(gu13,kmadd(gu12,kadd(R13,knmsub(K11,Km13,kmsub(K13,ksub(trKL,Km33),kmul(K12,Km23)))),kmadd(gu22,kadd(R23,knmsub(K12,Km13,kmsub(K23,ksub(trKL,Km33),kmul(K22,Km23)))),kmul(gu23,kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23)))))))))),kmadd(kadd(R23,knmsub(K12,Km13,kmsub(K23,ksub(trKL,Km33),kmul(K22,Km23)))),kmadd(gu13,kmadd(gu12,kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(gu22,kadd(R12,knmsub(K22,Km21,kmsub(K12,ksub(trKL,Km11),kmul(K23,Km31)))),kmul(gu23,kadd(R13,knmsub(K23,Km21,kmsub(K13,ksub(trKL,Km11),kmul(K33,Km31))))))),kmadd(gu23,kmadd(gu12,kadd(R12,knmsub(K11,Km12,kmsub(K12,ksub(trKL,Km22),kmul(K13,Km32)))),kmadd(gu22,kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(gu23,kadd(R23,knmsub(K13,Km12,kmsub(K23,ksub(trKL,Km22),kmul(K33,Km32))))))),kmul(gu33,kmadd(gu12,kadd(R13,knmsub(K11,Km13,kmsub(K13,ksub(trKL,Km33),kmul(K12,Km23)))),kmadd(gu22,kadd(R23,knmsub(K12,Km13,kmsub(K23,ksub(trKL,Km33),kmul(K22,Km23)))),kmul(gu23,kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23)))))))))),kmadd(kadd(R13,knmsub(K23,Km21,kmsub(K13,ksub(trKL,Km11),kmul(K33,Km31)))),kmadd(gu11,kmadd(gu13,kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(gu23,kadd(R12,knmsub(K22,Km21,kmsub(K12,ksub(trKL,Km11),kmul(K23,Km31)))),kmul(gu33,kadd(R13,knmsub(K23,Km21,kmsub(K13,ksub(trKL,Km11),kmul(K33,Km31))))))),kmadd(gu12,kmadd(gu13,kadd(R12,knmsub(K11,Km12,kmsub(K12,ksub(trKL,Km22),kmul(K13,Km32)))),kmadd(gu23,kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(gu33,kadd(R23,knmsub(K13,Km12,kmsub(K23,ksub(trKL,Km22),kmul(K33,Km32))))))),kmul(gu13,kmadd(gu13,kadd(R13,knmsub(K11,Km13,kmsub(K13,ksub(trKL,Km33),kmul(K12,Km23)))),kmadd(gu23,kadd(R23,knmsub(K12,Km13,kmsub(K23,ksub(trKL,Km33),kmul(K22,Km23)))),kmul(gu33,kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23)))))))))),kmul(kadd(R23,knmsub(K13,Km12,kmsub(K23,ksub(trKL,Km22),kmul(K33,Km32)))),kmadd(gu12,kmadd(gu13,kadd(R11,knmsub(K12,Km21,kmsub(K11,ksub(trKL,Km11),kmul(K13,Km31)))),kmadd(gu23,kadd(R12,knmsub(K22,Km21,kmsub(K12,ksub(trKL,Km11),kmul(K23,Km31)))),kmul(gu33,kadd(R13,knmsub(K23,Km21,kmsub(K13,ksub(trKL,Km11),kmul(K33,Km31))))))),kmadd(gu22,kmadd(gu13,kadd(R12,knmsub(K11,Km12,kmsub(K12,ksub(trKL,Km22),kmul(K13,Km32)))),kmadd(gu23,kadd(R22,knmsub(K12,Km12,kmsub(K22,ksub(trKL,Km22),kmul(K23,Km32)))),kmul(gu33,kadd(R23,knmsub(K13,Km12,kmsub(K23,ksub(trKL,Km22),kmul(K33,Km32))))))),kmul(gu23,kmadd(gu13,kadd(R13,knmsub(K11,Km13,kmsub(K13,ksub(trKL,Km33),kmul(K12,Km23)))),kmadd(gu23,kadd(R23,knmsub(K12,Km13,kmsub(K23,ksub(trKL,Km33),kmul(K22,Km23)))),kmul(gu33,kadd(R33,knmsub(K13,Km13,kmsub(K33,ksub(trKL,Km33),kmul(K23,Km23))))))))))))))))))),ToReal(4));
    
    CCTK_REAL_VEC KretschL CCTK_ATTRIBUTE_UNUSED = 
      kadd(term1,kadd(term2,term3));
    
    /* Copy local copies back to grid functions */
    vec_store_partial_prepare(i,vecimin,vecimax);
    vec_store_nta_partial(Kretsch[index],KretschL);
  }
  CCTK_ENDLOOP3STR(ML_Kretschmann_kretschmann);
}

extern "C" void ML_Kretschmann_kretschmann(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Entering ML_Kretschmann_kretschmann_Body");
  }
  
  if (cctk_iteration % ML_Kretschmann_kretschmann_calc_every != ML_Kretschmann_kretschmann_calc_offset)
  {
    return;
  }
  
  const char* const groups[] = {
    "ML_BSSN::ML_curv",
    "ML_BSSN::ML_log_confac",
    "ML_BSSN::ML_metric",
    "ML_BSSN::ML_trace_curv",
    "ML_Kretschmann::ML_Kretschmann"};
  GenericFD_AssertGroupStorage(cctkGH, "ML_Kretschmann_kretschmann", 5, groups);
  
  switch (fdOrder)
  {
    case 2:
    {
      GenericFD_EnsureStencilFits(cctkGH, "ML_Kretschmann_kretschmann", 1, 1, 1);
      break;
    }
    
    case 4:
    {
      GenericFD_EnsureStencilFits(cctkGH, "ML_Kretschmann_kretschmann", 2, 2, 2);
      break;
    }
    
    case 6:
    {
      GenericFD_EnsureStencilFits(cctkGH, "ML_Kretschmann_kretschmann", 3, 3, 3);
      break;
    }
    
    case 8:
    {
      GenericFD_EnsureStencilFits(cctkGH, "ML_Kretschmann_kretschmann", 4, 4, 4);
      break;
    }
    default:
      CCTK_BUILTIN_UNREACHABLE();
  }
  
  GenericFD_LoopOverInterior(cctkGH, ML_Kretschmann_kretschmann_Body);
  
  if (verbose > 1)
  {
    CCTK_VInfo(CCTK_THORNSTRING,"Leaving ML_Kretschmann_kretschmann_Body");
  }
}
