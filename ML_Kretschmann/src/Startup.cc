/*  File produced by Kranc */

#include "cctk.h"

extern "C" int ML_Kretschmann_Startup(void)
{
  const char* banner CCTK_ATTRIBUTE_UNUSED = "ML_Kretschmann";
  CCTK_RegisterBanner(banner);
  return 0;
}
